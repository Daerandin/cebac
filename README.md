# NAME

cebac - Compress Encrypt BACkup

## SYNOPSIS

Usage: cebac [options]

## DEPENDENCIES

libarchive  
gpgme  
libgcrypt  
Python3  
python-boto3 (python3 library required for uploading to AWS)  
python-dropbox (python3 library for uploading to dropbox)

## DESCRIPTION

**NOTE:** cebac is currently undergoing a major rewrite which will also implement proper testing. Until the rewrite is done, there will only be critical bugfixes.

Upload encrypted archives to cloud storage for off-site backup. Currently this program supports:
- AWS:
    - S3
    - glacier
    - deep archive
- Dropbox **Support for Dropbox will be dropped in the next version**
- custom upload script

All configuration options are set in the configuration file. There is an example configuration file provided which should be modified as needed, and put into ~/.config/cebac/cebac.conf
The cebac.conf file should be sufficiently documented for most cases.

The program will put all regular files and symlinks into the backup archives. Other kinds of special files are not included, and this is not something you need to worry about unless you are doing something weird.

For time consuming operations, cebac is able to resume at a later point if you had to abort the program. During operation, the program will write progress into the file ~/.config/cebac/resume
When next you run cebac, just ensure to apply the -r option to enable resuming. Resuming works by restarting at the previous operation, and cebac operation is split into three steps:

1. Make archive
2. Encrypt archive
3. Upload archive

Encryption is skipped if your configuration file is set to 'unencrypted', and uploading is skipped if you run without the -u option set.

Each archive is filled with files up to the maxsize defined in your configuration file. cebac will repeat the steps until all files specified for backup has been processed. If you abort, cebac will need to restart the step it was currently on when next you resume. This is very useful if you want to backup several hundred GB of data, which can take a very long time, and you need to power off your computer to resume at a later point.

Note that I made cebac for my own personal use. If you find it useful that's great. You can request features, and I might implement features if I think they make sense and would be useful.

### AWS DETAILS

In order to actually be able to upload stuff to AWS, you need to create the file: ~/.aws/credentials
This file must include the following:

```
[default]
aws_access_key_id = YOURKEYID
aws_secret_access_key = YOURSECRETACCESSKEY
```

To get the values you need, go to your IAM Management Console on AWS and create a user, which lets you get the security credentials.

### DROPBOX DETAILS

**NOTE:** Support for Dropbox will be removed in the next version. It is not in my interest to support it as I find it cumbersome.

You need to create an access token for your dropbox account. This can be done through your Dropbox App Console. You need to register a new app, and ensure you set the correct permission so that cebac can write to your dropbox. The specific permission you need to set is:

```
files.content.write
```

Other than this you just need an access token, which you must copy into the cebac configuration file. Dropbox access tokens are apparently only valid for 4 hours after generation. So if your backup operation is expected to take more time, then I suggest handling it in smaller segments. For instance, if you have several subdirectories in your backup files, then you might want to exclude some subdirectories in one operation to keep total time under 4 hours. Then get a new access token, and
then exclude previously completed subdirectories to backup the remaining data.

### CUSTOM SCRIPT DETAILS

If you wish to upload to a service that is currently not supported by cebac then you may use a custom script instead of one of the supported upload methods. There are a few requirements for such a custom script:

- It can either be a script or binary program, but in either case it must be executable.
- The script must accept a single argument, which will be the full path of the archive that is to be uploaded.
- It is important that the script does not delete or move the original archive after/during whatever operation it performs, cebac will delete it after successful operation of your script.
- Upon successful completion, it must return the value of 0 so that cebac knows is exited successfully.
- Upon failure, any non-zero value must be returned so cebac knows of the failure and will abort further operations. This will leave the archive intact so a new upload might be attempted by running cebac in 'resume' mode.

As cebac calls your custom script, it will print to standard output the exact line it attempts to run so that it is easier to debug any issues with your custom script.

## OPERATIONS

-c FILEPATH
    Use a different configuration file than the default at ~/.config/cebac/cebac.conf where FILEPATH is the custom file path.

-v  Print out version information and exit.

-p  Print out configuration options read from the configuration file, useful for verifying that your config file is correct.

-f  Print out every single file that will be included in the backup.

-a  Create archives according to the configuration file, this equals to step 1 and 2 in the description above

-u  Upload archives to cloud storage, this option requires that -a is also set.

-r  Resume a previously aborted operation. This will ignore whatever is in the configuration file as all options are read from the resume file from a previous operation that didn't fully complete.

-s  sha256 all archives and store the hashes along with corresponding filenames in a text file. This file will be in the same location as the archives. If you run with the -u option, then the hashes text file will also be uploaded to cloud storage. Even when running wityh -u, the hashes text file will remain locally on your computer.

-h  Print out help information and exit.

## IMPORTANT NOTES

For the most part, you are expected to be familiar with GnuPG and whatever cloud provider you use. If not, then you might be in for some surprises. With that in mind, here are some important points to be aware of:

- If you use password encryption, be sure you remember the password used for encrypting your archives. There is no known way to recover your backup if you forget the password.
- If you use key based encryption, then you MUST have the secret key so you are able to decrypt your archive later. If you are completely unfamiliar with gpg keys, I suggest using password encryption.
- You need to ensure you have a IAM user for AWS, and set up security credentials in the file specified in the DESCRIPTION above.
- AWS costs money based on how much you store there, make sure you fully understand how AWS bills you by fully reading the terms on their website.
- cebac is in no way responsible for any economic loss you suffer from the use of this software, cebac is only a tool to make it easier to upload to cloud storage.
- cebac is not affiliated with AWS or Dropbox, cebac only automates uploading to such services
- cebac is not made to download, decrypt and unpack archives. You need to do this yourself, either manually or with other tools.

Lastly: Make sure you fully understand billing and storage terms for your cloud service before you use cebac!

### NOTES ON ENCRYPTION

The two options available for encryption are:
- gpg\_key which will use your GPG public key. How strong this is depends entirely on your existing key.
- gpg\_pass which will encrypt symmetrically with a passphrase. The algorithm used is AES256, with digest SHA256. This is set with a dummy configuration file for gpg that will be used when cebac runs.

I am not expert in encryption, but I do want to point out that RSA, which is the common algorithm used for GPG keys, is assumed to be easily crackable with quantum computers. This has obviously not been verified yet because quantom computers is mostly theoretical, and actual prototypes are still far from being useful in a real world scenario.

The AES algorithm is considered to be quantum safe, as long as you use a strong passphrase. You can either set the passphrase directly in the configuration file, or you can leave it blank and you will instead be asked for the passphrase each time cebac encrypts an archive.

## BUILDING

On Linux (or any POSIX compliant/mostly compliant environment), ensure you have basic build tools installed (gcc, binutils, make) as well as the development headers for libarchive, gpgme, libgcrypt and Python3.

Then just enter the base directory for cebac and run:
```
./configure
make
sudo make install
```

You might want to run ./configure --prefix=/*preferred prefix* if you have a preferred install prefix. The last step with *make install* is also optional, it simply puts the compiled program, along with documentation, into the proper system paths.

boto3 and dropbox are Python3 libraries that must either be available system wide, or you can have it installed with pip install --user or simply use a virtualenv/venv that you use for running cebac. In any case, it is not required for building cebac, only for running when using aws or dropbox as upload type. If you only intend to use one form of upload service, then you only need to install the python library for your chosen service.


### WINDOWS INSTRUCTIONS

For MS Windows, you need to install [Cygwin](https://cygwin.com) ([MSYS2](https://msys2.org) also works, but this readme only has instructions for Cygwin) and during the install process, ensure to select the additional packages (usually you want to pick the greatest version number):

- binutils
- gcc-core
- libarchive-devel
- libassuan-devel
- libgpg-error-devel
- libgcrypt-devel
- make
- python3.9-devel
- python3.9-pip

You might also want to pick a text editor. I suggest also installing pax to unpack tar archives. You can use a different python3 version if desired and available.

Next, in cygwin, run the command: "pip3.9 install boto3 dropbox" (modify this if you use a different python version). If you only intend to use one form of upload service, then you only need to install the python library for your chosen service.

Now just build it normally according to the regular instructions.

Note that this executable can only be run from within the cygwin terminal. All of your windows drives are accessible via /cygdrive. I don't use windows myself, so for the most part you are on your own using this on an MS OS.

## AUTHOR

Daniel Jenssen <daerandin@gmail.com>

