% cebac(1)
% Daniel Jenssen <daerandin@gmail.com>
% 2022-08-25

# NAME
cebac - Compress Encrypt BACkup

# SYNOPSIS
**cebac** [*options*]

# DESCRIPTION
**cebac** creates encrypted archives and uploads it to a cloud service. This is useful if you have hundreds of GB you want to backup and you want to pack it into smaller archives. This will usually take a very long time, both compressing and encrypting archives can take a long time, and uploading will usually take much longer time. **cebac** can handle this and can even be aborted to resumed later, picking up on where it last left off.

Operations are handled in three stages:

- Creating archive with optional compression
- Encrypting the archive (optional step)
- Uploading archive (optional step)

If you have a total of 200 GB you want to backup, and want each archive to be maximum 50 GB in size, then the above mentioned three steps will be repeated four times. The process can be aborted at any time, and when you later resume the process then it will continue from the step it was previously on.

Most options are set in the configuration file, which will be located in your home directory under **.config/cebac/cebac.conf**

See the manpage for **cebac.conf**(5) for configuration details.

# OPTIONS
**-c** *filepath*
: Use *filepath* as the configuration file instead of the default file.

**-v**
: Print out version information

**-p**
: Print out all configuration options that have been parsed from the configuration file. Useful for verifying that you configured it correctly.

**-f**
: Print out every file that will be included in the backup, useful for verification purposes.

**-a**
: Create archive according to configuration options. This will also encrypt the archive if encryption is set in the configuration file.

**-u**
: Upload archives to cloud storage. This option requires *-a* to be set.

**-r**
: Resume a previously aborted operation.

**-s**
: Every archive will have the sha256 hash calculated. This hash, along with the corresponding filename, will be written to a text file. If you run with the *-u* option, then the text file with hashes will also be uploaded, while also having a copy remaining on your computer. This is very useful to be able to verify that backup archives have not become corrupt over time.

**-h**
: Print out help information.

# AWS DETAILS
**cebac** support uploading to *aws* and most options for this are set in **cebac.conf**(5)

There is also the need for the AWS credentials file which is used to authenticate you when performing uploads: **~/.aws/credentials** This file needs to contain three lines in the following format:

```
[default]
aws_access_key_id = YOURKEYID
aws_secret_access_key = YOURSECRETACCESSKEY
```
The required values for this file can be acquired from the IAM Management Console on AWS when you create a IAM user.

# DROPBOX DETAILS
**cebac** support uploading to *dropbox* and all options for this are set in **cebac.conf**(5)

In order to use *dropbox* you will need to go to your App Console for *dropbox* where you can register a new app. The important thing is that you enable write permissions. Specifically you need to enable: **files.content.write** in your App Console. Then you need to create an **access token** which is required for **cebac** to be allowed to upload to your account. Note that access tokens are valid for only 4 hours from creation, so plan the size of your total backup accordingly.

# CUSTOM SCRIPT DETAILS
**cebac** support the use of a custom script to handle the upload step. This can be useful if you want to upload to a service not supported by **cebac** and you are able to write the code to handle it yourself.

There are a few simple requirements for such a custom script:

- It can either be a script or binary program, but in either case it must be executable.
- The script must accept a single argument, which will be the full path of the archive that is to be uploaded.
- It is important that the script does not delete or move the original archive after/during whatever operation it performs, cebac will delete it after successful operation of your script.
- Upon successful completion, it must return the value of 0 so that cebac knows is exited successfully.
- Upon failure, any non-zero value must be returned so cebac knows of the failure and will abort further operations. This will leave the archive intact so a new upload might be attempted by running cebac in 'resume' mode.

The exact command used to call your custom script, along with the argument, will be printed to standard output. This might help in debugging issues with your code.

# EXAMPLES
**cebac -auf**
Print out all files for backup, then create archives and upload to cloud storage:

**cebac -c /path/to/cebac.conf -a**
Use a custom configuration file, create archives but don't upload:

**cebac -aur**
Resume a previous archiving/upload operation:

**cebac -p**
View configuration options but don't do anything else:

# LICENSE
**GPLv3+**: GNU GPL version 3 or later. This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

# SEE ALSO
**cebac.conf**(5)

