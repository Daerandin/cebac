% cebac.conf(5)
% Daniel Jenssen <daerandin@gmail.com>
% 2022-08-25

# NAME
cebac.conf - cebac configuration file

# SYNOPSIS
~/.config/cebac/cebac.conf

# DESCRIPTION
This is the configuration file for **cebac**(1) which by default should be located in **~/.config/cebac/cebac.conf** although a custom path can be defined. See **cebac**(1) for details.

An example configuration file is provided with the program which includes all options with examples.

All options regarding the operations of cebac is defined in this file. Every option in this file is defined as:

*variable* = *"value"*

The value can be enclosed in either single quotes **'** or double quotes **"** but it is important that use the same opening and closing quotes for a single value. Some variables take multiple values, then the values are separated by a comma **,**

# OPTIONS
**archive_type =**
: Archive format, currently supported is **tar**, **7zip**

**archive_name =**
: Name that the created archives will have, set any string you wish.

**archive_location =**
: Must be a valid absolute path to the directory you have write permissions to. This is where the created archives will be stored before upload is completed. If you don't run **cebac** with the *-u* option, then archives will simply remain in this location. If you use the *-s* option, then the text file containing the file hashes will also be stored in this location.

**compression_format =**
: Compression to be used on the archive, independent of that archive format. Currently supported is **none**, **xz**, **zstd**

**encryption_method =**
: How the archive will be encrypted, currently supported is:
    : - **unencrypted**
    : - **gpg_pass** Symmetric encryption using a passphrase. The algorithm used is AES256 with SAH256 as digest.
    : - **gpg_key** this will encrypt the archive with a public key, ensure you have the private key so that you can decrypt the archive in the future.

**gpg_recipient =**
: Identifier string of the GPG key that will be used to encrypt the archive. Make sure you possess the private key, otherwise you will not be able to decrypt the archive. This option will be ignored unless **encryption_method** is set to **gpg_key**.

**gpg_password =**
: The password that will be used to encrypt a symmetrically encrypted archive. This option will only be used if you have **encryption_method** set to **gpg_pass**. You can leave this blank, and you will instead be prompted to input a password when an archive is ready for encryption.

**upload_type =**
: Select supported upload method, currently supported is:
    : - **aws** upload to Amazon Web Services, requires additional options to be set.
    : - **dropbox** upload to Dropbox, requires additional options to be set.
    : - **custom** use a custom script/program for handling uploads, useful if you wish to code a method that is not supported. Requires additional options to be set.

**backup_files =**
: Files/directories that are to be archived. Can take multiple values, multiple values must be separated by a comma. All paths must be absolute paths.

**exclude_files =**
: Files/directories to be excluded from archiving. Can take multiple values, multiple values must be separated by a comma. All path must be absolute paths.

**max_size =**
: Maximum size of archives in bytes. In most cases, an archive will end up taking less space if you use compression. The only case where an archive can surpass this size is if you have any single files that are larger. You can also use suffixes to specify size larger than plain bytes: **K** for kibibytes (1024 bytes), **M** for mebibytes (1024 kibibytes), **G** for gibibytes (1024 mebibytes). For example: **50G**.

# AWS OPTIONS
**aws_type =**
: The type of AWS storage to upload the archives to. Supported types are: **s3**, **glacier**, **deep_archive**

**aws_name =**
: The name of your AWS bucket. This must be an existing AWS bucket.

# DROPBOX OPTIONS
**dropbox_token =**
: The access token for your dropbox account. Create this in your Dropbox App Console.

**dropbox_path =**
: The path in your dropbox where the archives will be stored. By default it is set to */* which is the root directory in Dropbox.

# CUSTOM OPTIONS
**script_path =**
: Absolute path to the custom script/binary that will handle uploads.

# ADVANCED OPTIONS
These are not mandatory, but will let you modify the details of the archive format/filter.

**xz_compression-level =**
: Must be set in the range *1 - 9* to specify the compression level for xz. Default is 6.

**xz_threads =**
: Number of CPU threads to use in xz compression. Default is 1. Note that increased threads will drastically increase memory requirement, while also drastically decreasing compression time.

**zstd_compression-level =**
: Must be set in range *1 - 22* to specify the compression level for zstd. Default is 3.

**7zip_compression =**
: Compression type specific to 7zip. Supported values: **store**, **deflate**, **bzip2**, **lzma1**, **lzma2**, **ppmd**. Default is lzma1. If you use anything except store, then you will want to use none for **compression_format**, otherwise the archive will be compressed twice, which takes a lot of time and gives no benefit.

**7zip_compression-level =**
: Must be set in range *0 - 9* to specify the compression level for 7zip. Default is 6.

# LICENSE
**GPLv3+**: GNU GPL version 3 or later. This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

# SEE ALSO
**cebac**(1)

