/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file handles the creation of archives with the help of libarchive. */

#include <archive_entry.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "mkarchive.h"
#include "error_handling.h"

/* create_tar - uses libarchive to create a tar archive of the pax format.
 * Takes three arguments:
 * files - an array of strings, each string is one file
 * num - number of files
 * cfilter - compression filter to be used on the archive
 * apath - full path to the archive that is to be created
 *
 * format_options is a void pointer to a struct containing additional options
 * for the format used. It must be a struct corresponding to the format used.
 *
 * filter_options is a void pointer to a strict containing additional options
 * for the filter used. It must be a struct corresponding to the filter used.
 * 
 * The last two arguments are optional and can be NULL. This function will not
 * test the correctness of the values provided.
 *
 */
void
create_archive(struct stringlist *files,
               const char *apath,
               char **return_name,
               bool overwrite,
               int (*format_func)(struct archive *),
               int (*filter_func)(struct archive *),
               const char *optionstring,
               const char *format_suffix,
               const char *filter_suffix)
{
    struct archive *a;
    struct archive_entry *entry;
    struct stat st;
    char buff[8192];
    int res, len, fd;
    char *linkbuf, *outfile = NULL;
    const char errmsg[] = "mkarchive.c:create_tar";
    unsigned int stringsize = strlen(apath) + 1;

    if (format_suffix)
        stringsize += strlen(format_suffix);
    if (filter_suffix)
        stringsize += strlen(filter_suffix);
    outfile = malloc(stringsize);
    if (!outfile)
        error_and_exit(errmsg, errno, NULL);
    strcpy(outfile, apath);
    if (format_suffix)
        strcat(outfile, format_suffix);
    if (filter_suffix)
        strcat(outfile, filter_suffix);

    a = archive_write_new();

    if (format_func)
        format_func(a);
    if (filter_func)
        filter_func(a);
    if (optionstring)
        archive_write_set_options(a, optionstring);

    /* Test if a file already exists. */
    errno = 0;
    res = stat(outfile, &st);
    if (errno != ENOENT && !overwrite)
        cerror_and_exit(errmsg, "Archive file already exists: ", outfile);
    else if (overwrite && !res)
        remove(outfile);
    if (archive_write_open_filename(a, outfile) != ARCHIVE_OK)
        cerror_and_exit(errmsg, archive_error_string(a), NULL);
    entry = archive_entry_new();
    while (files) {
        res = lstat(files->filepathname, &st);
        if (res == -1 && errno == ENOENT) {
            /* If a file does not exist, we silently ignore it. */
            files = files->next;
            continue;
        } else if (res == -1 && errno != ENOENT)
            error_and_exit(errmsg, errno, files->filepathname);
        archive_entry_clear(entry);
        archive_entry_set_pathname(entry, files->filepathname);
        archive_entry_copy_stat(entry, &st);
        if (S_ISDIR(st.st_mode))
            archive_entry_set_perm(entry, 0755);
        else
            archive_entry_set_perm(entry, 0644);
        /* If symlink, we need to include link destination. */
        if (S_ISLNK(st.st_mode)) {
            linkbuf = malloc(st.st_size + 1);
            if (!linkbuf)
                error_and_exit(errmsg, errno, NULL);
            readlink(files->filepathname, linkbuf, st.st_size);
            linkbuf[st.st_size] = '\0';
            archive_entry_set_symlink(entry, linkbuf);
            free(linkbuf);
        }
        archive_write_header(a, entry);
        if (S_ISREG(st.st_mode)) {
            fd = open(files->filepathname, O_RDONLY);
            if (fd == -1)
                error_and_exit(errmsg, errno, files->filepathname);
            len = read(fd, buff, sizeof(buff));
            if (len == -1)
                error_and_exit(errmsg, errno, files->filepathname);
            while (len > 0) {
                res = archive_write_data(a, buff, len);
                if (res < 0)
                    cerror_and_exit(errmsg, archive_error_string(a), NULL);
                len = read(fd, buff, sizeof(buff));
                if (len == -1)
                    error_and_exit(errmsg, errno, files->filepathname);
            }
            close(fd);
        }
        files = files->next;
    }
    archive_entry_free(entry);
    res = archive_write_close(a);
    if (res == ARCHIVE_FATAL)
        cerror_and_exit(errmsg, archive_error_string(a), NULL);
    *return_name = outfile;
    archive_write_free(a);
}
