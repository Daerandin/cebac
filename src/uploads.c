/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file handles the actual upload to cloud storage. */

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include "uploads.h"
#include "error_handling.h"

/* upload_to_aws - uploads a file to AWS
 * file - is a pointer to a string containing the full path to the file to be
 * uploaded.
 * name - is a pointer to a string containing the AWS bucket/vault name.
 * class - is a pointer to a string containing the storage class.
 * program_name - ponter to a string containing the base program name.
 * Valid storage classes are:
 *      S3
 *      GLACIER
 *      DEEP_ARCHIVE
 *
 */
void
upload_to_aws(const char *file,
              const char *name,
              const char *class)
{
    int i, classlen, result;
    const char errmsg[] = "uploads.c:upload_to_aws";
    struct stat buf;
    const char *valid_class[] = {
                                "STANDARD",
                                "GLACIER",
                                "DEEP_ARCHIVE",
    };

    /* This is the python code to upload our file. */
    const char python_string1[] =
    "import boto3, os\n"
    "s3_client = boto3.client('s3')\n";
    const char python_string2[] = "filepath = '";
    const char python_string3[] = "'\nname = '";
    const char python_string4[] = "'\nstorageclass = '";
    const char python_string5[] =
    "'\nresponse = s3_client.upload_file(filepath, name, "
    "os.path.basename(filepath), ExtraArgs={'StorageClass': storageclass})\n";
    char *python_script;

    /* Test that file is a valid file. */
    result = stat(file, &buf);
    if (result == -1)
        error_and_exit(errmsg, errno, file);
    /* Test that class is valid. */
    classlen = sizeof(valid_class) / sizeof(valid_class[0]);
    for (i = 0; i < classlen; i++) {
        if (!strcmp(class, valid_class[i]))
                break;
    }
    if (i == classlen)
        cerror_and_exit(errmsg, "class argument is not a valid StorageClass "
                        "argument: ", class);
    /* Create the full python script. */
    python_script = malloc(strlen(python_string1) + strlen(python_string2) +
                           strlen(python_string3) + strlen(python_string4) +
                           strlen(python_string5) + strlen(file) +
                           strlen(name) + strlen(class) + 1);
    if (!python_script)
        error_and_exit(errmsg, errno, NULL);
    strcpy(python_script, python_string1);
    strcat(python_script, python_string2);
    strcat(python_script, file);
    strcat(python_script, python_string3);
    strcat(python_script, name);
    strcat(python_script, python_string4);
    strcat(python_script, class);
    strcat(python_script, python_string5);

    result = PyRun_SimpleString(python_script);
    free(python_script);
    if (result == -1)
        cerror_and_exit(errmsg, "could not perform upload.", NULL);
}

/* run_custom_script - handle the archive with custom user provided script
 * file - pointer to a string with the full path and name of the archive that
 * the custom user provided script will handle in some way.
 * custom_script - pointer to a string containing the full path to the custom
 * script that is to be run with *file as a the only argument.
 *
 * The custom script must take a single argument, a filepath. It must return
 * the value of 0 on success, or 1 on failure.
 *
 */
void
run_custom_script(const char *file, const char *custom_script)
{
    int result, cmdlength = 0;
    const char errmsg[] = "uploads.c:run_custom_script";
    char *fullcmd = NULL;
    struct stat buf;

    /* Check for NULL pointer, or empty string in both arguments. */
    if (!file || strlen(file) == 0)
        cerror_and_exit(errmsg, "No file argument provided.", NULL);
    if (!custom_script || strlen(custom_script) == 0)
        cerror_and_exit(errmsg, "No custom_script argument provided.", NULL);

    /* Test that file is a valid file. */
    result = stat(file, &buf);
    if (result == -1)
        error_and_exit(errmsg, errno, file);

    /* Perform same test for the custom_script. */
    result = stat(custom_script, &buf);
    if (result == -1)
        error_and_exit(errmsg, errno, file);

    cmdlength = strlen(file) + 3;
    cmdlength += strlen(custom_script) + 1;
    fullcmd = malloc(cmdlength);
    if (!fullcmd)
        error_and_exit(errmsg, errno, NULL);
    strcpy(fullcmd, custom_script);
    strcat(fullcmd, " \"");
    strcat(fullcmd, file);
    strcat(fullcmd, "\"");
    /* To help users debug potential issues with their script. */
    printf("Running user supplied custom script with "
           "argument as follows:\n%s\n", fullcmd);
    result = system(fullcmd);
    if (result)
        cerror_and_exit(errmsg, "Failed while running custom script: ",
                        fullcmd);
    free(fullcmd);
}

/* upload_to_dropbox - upload an archive to dropbox
 * file - pointer to a string containing the full path and name of the archive
 * that is to be uploaded.
 * token - pointer to a string containing the access token for a dropbox
 * account.
 * path - the path in your dropbox where to upload to.
 *
 */
void
upload_to_dropbox(const char *file,
                  const char *token,
                  const char *path)
{
    char *fullpath, *python_script;
    int i, lastslash, result, pathlen = 1;
    struct stat buf;
    char charbuf[20];
    const char errmsg[] = "uploads.c:upload_to_dropbox";

    const char python_string1[] = "import dropbox\n"
                                  "import stone\naccess_token = '";
    const char python_string2[] = "'\nfile_size = ";
    const char python_string3[] = "\ndbx = dropbox.Dropbox(access_token, "
                                  "timeout=120)\nchunk_size = 4 * 1024 * 1024"
                                  "\nf = open('";
    const char python_string4[] = "', 'rb')\nif file_size <= chunk_size:\n\t"
                                  "try:\n\t\tdbx.files_upload(f.read(), '";
    const char python_string5[] = "')\n\texcept stone.backends.python_rsrc."
                                  "stone_validators.ValidationError:"
                                  "\n\t\tte = True\nelse:\n\t"
                                  "upload_session = dbx.files_up"
                                  "load_session_start(f.read(chunk_size))\n\t"
                                  "cursor = dropbox.files.UploadSessionCursor"
                                  "(session_id=upload_session.session_id, "
                                  "offset=f.tell())\n\tcommit = "
                                  "dropbox.files.CommitInfo(path='";
    const char python_string6[] = "')\n\twhile f.tell() < file_size:\n\t\t"
                                  "if ((file_size - f.tell()) <= chunk_size):"
                                  "\n\t\t\ttry:\n\t\t\t\tdbx.files_upload_sess"
                                  "ion_finish(f.read(chunk_size), cursor, com"
                                  "mit)\n\t\t\texcept stone.backends.python_r"
                                  "src.stone_validators.ValidationError:"
                                  "\n\t\t\t\tte = True\n"
                                  "\t\telse:\n\t\t\tdbx."
                                  "files_upload_session_append(f.read(chunk_s"
                                  "ize), cursor.session_id, cursor.offset)\n"
                                  "\t\t\tcursor.offset = f.tell()\n";
    lastslash = 0;
    for (i = 0; i < (int)strlen(file); i++) {
        if (file[i] == '/')
            lastslash = i + 1;
    }
    if (path[strlen(path) - 1] != '/')
        pathlen++;
    pathlen += strlen(&file[lastslash]) + strlen(path) + 1;
    fullpath = malloc(pathlen);
    if (!fullpath)
        error_and_exit(errmsg, errno, NULL);
    strcpy(fullpath, path);
    if (path[strlen(path) - 1] != '/')
        strcat(fullpath, "/");
    strcat(fullpath, &file[lastslash]);

    /* Handle filesize, we are setting the cap at 250 GiB to stay safely
     * within the limits of Dropbox. */
    result = stat(file, &buf);
    if (result == -1)
        error_and_exit(errmsg, errno, file);
    if (buf.st_size > 250ll * 1024ll * 1024ll * 1024ll)
        cerror_and_exit(errmsg, "Dropbox limits require smaller archive "
                        "size. Limit of 250 GiB.", NULL);
    snprintf(charbuf, 19, "%lld", (long long)buf.st_size);

    python_script = malloc(strlen(python_string1) + strlen(python_string2) +
                           strlen(python_string3) + strlen(python_string4) +
                           strlen(python_string5) + strlen(python_string6) +
                           strlen(charbuf) + strlen(file) + strlen(fullpath) +
                           strlen(token) + strlen(fullpath) + 1);
    if (!python_script)
        error_and_exit(errmsg, errno, NULL);
    strcpy(python_script, python_string1);
    strcat(python_script, token);
    strcat(python_script, python_string2);
    strcat(python_script, charbuf);
    strcat(python_script, python_string3);
    strcat(python_script, file);
    strcat(python_script, python_string4);
    strcat(python_script, fullpath);
    strcat(python_script, python_string5);
    strcat(python_script, fullpath);
    strcat(python_script, python_string6);
    free(fullpath);

    result = PyRun_SimpleString(python_script);
    free(python_script);
    if (result == -1)
        cerror_and_exit(errmsg, "could not perform upload.\n", NULL);
}

void
init_python(const char *pname)
{
    PyStatus status;
    PyConfig config;
    PyConfig_InitPythonConfig(&config);

    status = PyConfig_SetBytesString(&config, &config.program_name, pname);
    if (PyStatus_Exception(status))
        goto exception;

    status = Py_InitializeFromConfig(&config);
    if (PyStatus_Exception(status))
        goto exception;

    PyConfig_Clear(&config);
    return;

exception:
    PyConfig_Clear(&config);
    Py_ExitStatusException(status);
}

void
destroy_python(void)
{
    if (Py_FinalizeEx() < 0)
        cerror_and_exit("uploads.c:destroy_python",
                        "could not finalize Python instance.\n", NULL);
}
