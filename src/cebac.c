/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file contains most of the logic of the cebac program. Functions here
 * are inteded to be called from main.
 */

#define HASH_SUFFIX "-sha256.txt"

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cebac.h"
#include "stringlines.h"
#include "mkarchive.h"
#include "encryption.h"
#include "error_handling.h"
#include "uploads.h"
#include "hashing.h"

static void parse_configuration_options(
        struct configuration *,
        const struct vlines *,
        bool);
static int number_of_values(const struct valuelist *);
static int verify_configuration(const struct configuration *);
static void make_archive(struct configuration *, bool);
static void write_progress(struct configuration *, const char *);
static long long write_files_binary(struct files *);
static struct files *read_files_binary(long long);


/* cversion - print simple version and copyright statement. */
void
cversion(void)
{
    printf(
"%s\n"
"Copyright (C) %s Daniel Jenssen\n"
"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
"\nThis is free software: you are free to change and redistribute it.\n"
"There is NO WARRANTY, to the extent permitted by law.\n",
PACKAGE_STRING, YEAR);
}

/* populate_configuration - takes two arguments
 * conf is a pointer to struct configuration, all members must be initialized
 * to either -1 or NULL depending on type.
 * cfile is the path to the configuration file that all configuration options
 * will be read from.
 *
 * This function will dynamically allocate memory for the pointers in the
 * struct configuration, it is up to the calling function to ensure this
 * memory is freed later. It is suggested to use the free_configuration()
 * function for this purpose.
 *
 */
void
populate_configuration(struct configuration *conf,
                       const char *cfile,
                       bool resume)
{
    FILE *fp = NULL;
    char *fbuffer = NULL;
    struct stat buf;
    struct slines *tmp, *config_lines = NULL;
    struct vlines *varval = NULL;
    int c, result, i = 0;
    const char errmsg[] = "cebac.c:populate_configuration";

    if (!cfile)
        cerror_and_exit(errmsg, "Missing path to configuration file.", NULL);

    /* Now a long list of testing for errors after doing simple stuff. */
    result = stat(cfile, &buf);
    if (result == -1)
        error_and_exit(errmsg, errno, cfile);
    if (!S_ISREG(buf.st_mode))
        cerror_and_exit(errmsg, "Configuraton file is not a regular file.",
                        cfile);
    fbuffer = malloc(buf.st_size + 1);
    if (!fbuffer)
        error_and_exit(errmsg, errno, NULL);

    fp = fopen(cfile, "r");
    if (!fp)
        error_and_exit(errmsg, errno, cfile);

    /* Now we have successfully opened the configuration file so we read
     * the entire contents into fbuffer, which has been allocated to be large
     * enough to contain the entire file. */

    while ((c = fgetc(fp)) != EOF)
        fbuffer[i++] = c;
    fbuffer[i] = '\0';

    result = fclose(fp);

    if (result == EOF)
        error_and_exit(errmsg, errno, cfile);

    /* Split the string into seperate lines. */
    config_lines = string_to_lines_destructive(fbuffer);

    varval = extract_variable_value(config_lines);

    /* We can now free some previously allocated memory. */
    while (config_lines) {
        tmp = config_lines;
        config_lines = config_lines->next;
        free(tmp);
    }
    free(fbuffer);

    parse_configuration_options(conf, varval, resume);

    clear_vlines(varval);
    result = verify_configuration(conf);
    if (!result)
        exit(EXIT_FAILURE);

    if (!resume) {
        conf->expanded = recursively_expand(conf->bfiles, conf->xfiles);
        if (!conf->expanded)
            cerror_and_exit(errmsg, conf->bfiles->filepathname,
                    " is not a valid file or path.");
        conf->expanded = sort_files(conf->expanded, false);
        remove_duplicates(conf->expanded);
    }

}

/* parse_configuration_options - takes three arguments
 *
 * conf is a pointer to a struct configuration, this is where all the options
 * will be stored.
 *
 * conflines is a pointer to a struct vlines, defined in the stringlines.h
 * file. This struct contains variables and associated values along with the
 * line number they were read from. This all comes from the configuration file
 * for this program.
 *
 * resume indicates if we are resuming, in which case more variable/values are
 * valid for reading.
 *
 * The function only does some limited checks for errors, so all configuration
 * options are not guaranteed to be correct.
 *
 */
static void
parse_configuration_options(
        struct configuration *conf,
        const struct vlines *conflines,
        bool resume)
{
    struct valuelist *ptr = NULL;
    int i, vlen;
    long tlen, checkval;
    long long nfiles;
    const char errmsg[] = "cebac.c:parse_configuration_options";
    const char *optr;
    char errbuff[4];
    bool varerror;
    struct stringlist *current, *tmp;
    char *endptr = NULL;
    current = NULL;
    tmp = NULL;

    const char *varnames[] = {
                            /*  0 */ "archive_type",
                            /*  1 */ "compression_format",
                            /*  2 */ "encryption_method",
                            /*  3 */ "gpg_recipient",
                            /*  4 */ "gpg_password",
                            /*  5 */ "upload_type",
                            /*  6 */ "script_path",
                            /*  7 */ "aws_type",
                            /*  8 */ "aws_name",
                            /*  9 */ "dropbox_token",
                            /* 10 */ "dropbox_path",
                            /* 11 */ "backup_files",
                            /* 12 */ "exclude_files",
                            /* 13 */ "max_size",
                            /* 14 */ "archive_name",
                            /* 15 */ "archive_location",
                            /* 16 */ "archive_cur",
                            /* 17 */ "afile",
                            /* 18 */ "expanded",
                            /* 19 */ "stage",
                            /* 20 */ "xz_compression-level",
                            /* 21 */ "xz_threads",
                            /* 22 */ "zstd_compression-level",
                            /* 23 */ "7zip_compression",
                            /* 24 */ "7zip_compression-level",
                            };

    if (!conf)
        cerror_and_exit(errmsg,"struct configuration pointer was NULL.", NULL);
    if (!conflines)
        cerror_and_exit(errmsg,"struct vlines pointer was NULL.", NULL);

    vlen = sizeof(varnames) / sizeof(varnames[0]);

    while (conflines) {
        for (i = 0; i < vlen; i++) {
            if (!strcmp(varnames[i], conflines->variable))
                break;
        }
        if (i == vlen ||
            ((i == 16 || i == 17 || i == 18 || i == 19) && !resume))
            cerror_and_exit(errmsg,
                            "Unrecognized variable in configuration file: ",
                            conflines->variable);
        /* For all other cases than the backup/exclude files, we must only
         * have a single value. */
        if (i != 11 && i != 12) {
            if (number_of_values(conflines->values) != 1) {
                if (conflines->line_number > 999)
                    cerror_and_exit(errmsg, "Too many variables.", NULL);
                snprintf(errbuff, 4, "%d", conflines->line_number);
                cerror_and_exit(errmsg, "Too many variables found in line: ",
                                errbuff);
            }
        }

        varerror = false;
        switch (i) {
            case 0: /* archive_type */
                set_format(conf, conflines->values->value);
                break;
            case 1: /* compression_format */
                set_filter(conf, conflines->values->value);
                break;
            case 2: /* encryption_method */
                if (!strcmp(conflines->values->value, "unencrypted"))
                    conf->encryption_method = unencrypted;
                else if (!strcmp(conflines->values->value, "gpg_pass"))
                    conf->encryption_method = gpg_pass;
                else if (!strcmp(conflines->values->value, "gpg_key"))
                    conf->encryption_method = gpg_key;
                else
                    varerror = true;
                break;
            case 3: /* gpg_recipient */
                if (strlen(conflines->values->value) > 0) {
                    conf->gpg_recipient = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->gpg_recipient)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->gpg_recipient, conflines->values->value);
                }
                break;
            case 4: /* gpg_password */
                if (strlen(conflines->values->value) > 0) {
                    conf->gpg_password = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->gpg_password)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->gpg_password, conflines->values->value);
                }
                break;
            case 5: /* upload_type */
                if (!strcmp(conflines->values->value, "aws"))
                    conf->upload_t = aws;
                else if (!strcmp(conflines->values->value, "custom"))
                    conf->upload_t = script;
                else if (!strcmp(conflines->values->value, "dropbox"))
                    conf->upload_t = dropbox;
                else
                    varerror = true;
                break;
            case 6: /* script_path */
                if (strlen(conflines->values->value) > 0) {
                    conf->script_path = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->script_path)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->script_path, conflines->values->value);
                }
                break;
            case 7: /* aws_type */
                if (!strcmp(conflines->values->value, "s3"))
                    conf->aws_type = s3;
                else if (!strcmp(conflines->values->value, "glacier"))
                    conf->aws_type = glacier;
                else if (!strcmp(conflines->values->value, "deep_archive"))
                    conf->aws_type = deep_archive;
                else
                    varerror = true;
                break;
            case 8: /* aws_name */
                if (strlen(conflines->values->value) > 0) {
                    conf->aws_name = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->aws_name)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->aws_name, conflines->values->value);
                }
                break;
            case 9: /* dropbox_token */
                if (strlen(conflines->values->value) > 0) {
                    conf->dropbox_token = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->dropbox_token)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->dropbox_token, conflines->values->value);
                }
                break;
            case 10: /* dropbox_path */
                if (strlen(conflines->values->value) > 0) {
                    conf->dropbox_path = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->dropbox_path)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->dropbox_path, conflines->values->value);
                }
                break;
            case 11: /* backup_files */
                ptr = conflines->values;
                while (ptr) {
                    if (strlen(ptr->value) > 0) {
                        tmp = malloc(sizeof(struct stringlist));
                        if (!tmp)
                            error_and_exit(errmsg, errno, NULL);
                        if (!conf->bfiles) {
                            conf->bfiles = tmp;
                            current = tmp;
                        } else {
                            current->next = tmp;
                            current = current->next;
                        }
                        current->next = NULL;
                        tlen = strlen(ptr->value);
                        if (ptr->value[tlen-1] == FILEPATH_SEP[0]) {
                            ptr->value[tlen-1] = '\0';
                            tlen--;
                        }
                        current->filepathname = malloc(tlen + 1);
                        if (!current->filepathname)
                            error_and_exit(errmsg, errno, NULL);
                        strcpy(current->filepathname, ptr->value);
                    }
                    ptr = ptr->next;
                }
                break;
            case 12: /* exclude_files */
                ptr = conflines->values;
                while (ptr) {
                    if (strlen(ptr->value) > 0) {
                        tmp = malloc(sizeof(struct stringlist));
                        if (!tmp)
                            error_and_exit(errmsg, errno, NULL);
                        if (!conf->xfiles) {
                            conf->xfiles = tmp;
                            current = tmp;
                        } else {
                            current->next = tmp;
                            current = current->next;
                        }
                        current->next = NULL;
                        tlen = strlen(ptr->value);
                        if (ptr->value[tlen-1] == FILEPATH_SEP[0]) {
                            ptr->value[tlen-1] = '\0';
                            tlen--;
                        }
                        current->filepathname = malloc(tlen + 1);
                        if (!current->filepathname)
                            error_and_exit(errmsg, errno, NULL);
                        strcpy(current->filepathname, ptr->value);
                    }
                    ptr = ptr->next;
                }
                break;
            case 13: /* max_size */
                errno = 0;
                conf->max_size = strtoll(conflines->values->value,
                        &endptr, 10);
                if (errno)
                    error_and_exit(errmsg, errno, "max_size");
                if (!(endptr && !*endptr)) {
                    if (strlen(endptr) > 1)
                        varerror = true;
                    else {
                        switch (*endptr) {
                            case 'G':
                                conf->max_size *= 1024;
                                /* fall through */
                            case 'M':
                                conf->max_size *= 1024;
                                /* fall through */
                            case 'K':
                                conf->max_size *= 1024;
                                break;
                            default:
                                varerror = true;
                        }
                    }
                }
                break;
            case 14: /* archive_name */
                if (strlen(conflines->values->value) > 0) {
                    conf->archive_name = malloc(
                            strlen(conflines->values->value) + 1);
                    if (!conf->archive_name)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->archive_name, conflines->values->value);
                }
                break;
            case 15: /* archive_location */
                if (strlen(conflines->values->value) > 0) {
                    ptr = conflines->values;
                    tlen = strlen(ptr->value);
                    if (ptr->value[tlen - 1] == FILEPATH_SEP[0]) {
                        ptr->value[tlen - 1] = '\0';
                        tlen--;
                    }
                    conf->archive_location = malloc(tlen + 1);
                    if (!conf->archive_location)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->archive_location, conflines->values->value);
                }
                break;
            case 16: /* archive_cur */
                errno = 0;
                checkval = strtol(conflines->values->value, NULL, 10);
                if (errno)
                    error_and_exit(errmsg, errno, "archive_cur");
                if (checkval <= INT_MAX)
                    conf->archive_cur = (int)checkval;
                else
                    cerror_and_exit(errmsg, "archive_cur is too large.", NULL);
                break;
            case 17: /* afile */
                if (strlen(conflines->values->value) > 0) {
                    conf->afile = malloc(strlen(conflines->values->value)
                                         + 1);
                    if (!conf->afile)
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(conf->afile, conflines->values->value);
                }
                break;
            case 18: /* expanded */
                errno = 0;
                nfiles = strtoll(conflines->values->value, NULL, 10);
                if (errno)
                    error_and_exit(errmsg, errno, "expanded");
                conf->expanded = read_files_binary(nfiles);
                break;
            case 19: /* stage */
                if (!strcmp(conflines->values->value, "first_archive"))
                    conf->stage = first_archive;
                else if (!strcmp(conflines->values->value, "second_encrypt"))
                    conf->stage = second_encrypt;
                else if (!strcmp(conflines->values->value, "third_upload"))
                    conf->stage = third_upload;
                else
                    varerror = true;
                break;
            case 20: /* xz_compression-level */
                /* fall through */
            case 21: /* xz_threads */
                /* fall through */
            case 22: /* zstd_compression-level */
                optr = conflines->variable;
                while (*(optr++) != '_');
                set_filter_option(conf, optr, conflines->values->value,
                                  conflines->variable);
                break;
            case 23: /* 7zip_compression */
                /* fall through */
            case 24: /* 7zip_compression-level */
                optr = conflines->variable;
                while (*(optr++) != '_');
                set_format_option(conf, optr, conflines->values->value,
                                  conflines->variable);
                break;
        }
        if (varerror) {
            if (conflines->line_number > 999)
                cerror_and_exit(errmsg, "Unrecognized value in configuration "
                                "file.", NULL);
            snprintf(errbuff, 4, "%d", conflines->line_number);
            cerror_and_exit(errmsg, "Unrecognized value in configuration file"
                            " on line number: ", errbuff);
        }
        conflines = conflines->next;
    }
}

/* Calculate the number of values stored in the struct valuelist. */
static int
number_of_values(const struct valuelist *values)
{
    int i = 0;

    while (values) {
        i++;
        values = values->next;
    }

    return i;
}

/* Verifies that all required configuration options are set.
 * Returns 1 to indicate that all options are good.
 * On any error, a message will be printed to sdterr, and return 0.
 */
static int
verify_configuration(const struct configuration *conf)
{
    const char derror[] = "Error in configuration file: ";
    struct stat buf;
    int res;
    if (!conf) {
        fprintf(stderr, "Error in cebac.c:verify_configuration\nPointer to "
                "struct configuration is NULL.\n");
        return 0;
    }

    switch (conf->format_options.type_id.archive_type) {
        case tar:
            break;
        case s7zip:
            break;
        default:
            fprintf(stderr, "%sarchive_type is missing.\n", derror);
            return 0;
    }
    switch (conf->format_options.type_id.compression_format) {
        case none:
            break;
        case xz:
            break;
        case zstd:
            break;
        default:
            fprintf(stderr, "%scompression_format is missing.\n", derror);
            return 0;

    }
    switch (conf->encryption_method) {
        case unencrypted:
            break;
        case gpg_pass:
            break;
        case gpg_key:
            if (!conf->gpg_recipient) {
                fprintf(stderr, "%sEncryption method set to gpg_key, but no "
                        "gpg_recipient set.\n", derror);
                return 0;
            }
            break;
        default:
            fprintf(stderr, "%sencryption_method is missing.\n", derror);
            return 0;
    }
    switch (conf->upload_t) {
        case aws:
            switch (conf->aws_type) {
                case s3:
                    break;
                case glacier:
                    break;
                case deep_archive:
                    break;
                default:
                    fprintf(stderr, "%saws_type is missing.\n", derror);
                    return 0;
            }
            if (!conf->aws_name) {
                fprintf(stderr, "%saws_name is missing.\n", derror);
                return 0;
            }
            break;
        case script:
            if (!conf->script_path) {
                fprintf(stderr, "%scustom script path is missing.\n", derror);
                return 0;
            }
            break;
        case dropbox:
            if (!conf->dropbox_token) {
                fprintf(stderr, "%sdropbox_token is missing.\n", derror);
                return 0;
            }
            if (!conf->dropbox_path) {
                fprintf(stderr, "%sdropbox_path is missing.\n", derror);
                return 0;
            }
            break;
        default:
            fprintf(stderr, "%supload_type is incorrect.\n", derror);
            return 0;
    }
    if (!(conf->bfiles)) {
        fprintf(stderr, "%sno backup_files specified.\n", derror);
        return 0;
    }
    if (conf->max_size < 1) {
        fprintf(stderr, "%smax_size is not set.\n", derror);
        return 0;
    }
    if (!conf->archive_name) {
        fprintf(stderr, "%sarchive_name is missing.\n", derror);
        return 0;
    }
    if (!conf->archive_location) {
        fprintf(stderr, "%sarchive_location is missing.\n", derror);
        return 0;
    } else {
        res = stat(conf->archive_location, &buf);
        if (res == -1) {
            fprintf(stderr, "%scan't stat archive_location: %s\n", derror,
                    conf->archive_location);
            return 0;
        }
        if (!S_ISDIR(buf.st_mode)) {
            fprintf(stderr, "%sarchive_location is not a directory: %s\n",
                    derror, conf->archive_location);
            return 0;
        }
    }
    return 1;
}

/* make_archive - creates list of files, which is less than max_size. Then
 * invokes the create_archive function from mkarchive.h.
 *
 * The expanded member of conf will be changed during this. Each element put
 * into the archive, will be removed from the expanded list. The struct
 * element will be freed, but the filepathname will be moved into a temporary
 * array, and freed after use.
 *
 */
static void
make_archive(struct configuration *conf, bool resume)
{
    struct stringlist *beginning, *ptr, *tmp;
    char *apath, *rpath = NULL;
    char suffix[20];
    const char errmsg[] = "cebac.h:make_archive";
    long long left = conf->max_size;
    struct files *start, *prev, *current;
    char *optstring;

    sprintf(suffix, "-%d", conf->archive_cur);

    apath = malloc(strlen(conf->archive_location) +
                   strlen(conf->archive_name) +
                   strlen(suffix) +
                   strlen(FILEPATH_SEP) + 1);
    if (!apath)
        error_and_exit(errmsg, errno, NULL);
    strcpy(apath, conf->archive_location);
    strcat(apath, FILEPATH_SEP);
    strcat(apath, conf->archive_name);
    strcat(apath, suffix);
    
    start = conf->expanded;
    current = start;
    beginning = NULL;
    ptr = NULL;
    /* First get how many objects we need space for. */
    while (current) {
        if ( !beginning || current->fsize < left) {
            left -= current->fsize;
            tmp = malloc(sizeof(struct stringlist));
            if (!tmp)
                error_and_exit(errmsg, errno, NULL);
            tmp->filepathname = current->filepathname;
            tmp->next = NULL;
            if (!beginning) {
                beginning = tmp;
                ptr = tmp;
            } else {
                ptr->next = tmp;
                ptr = tmp;
            }
            tmp = NULL;
        }
        current = current->next;
    }
    /* Now we have a list conforming to max_size, so we will send it off to
     * create_archive. */
    set_archive_options(conf, &optstring);
    create_archive(beginning, apath, &rpath, resume,
                   conf->format_options.set_func,
                   conf->filter_options.set_func,
                   optstring,
                   conf->format_options.suffix,
                   conf->filter_options.suffix);
    free(optstring);
    free(apath);
    /* At this point, everything went according to plan, and we can free
     * elements from the struct files list since they have successfully been
     * written to the archive. */
    ptr = beginning;
    while (ptr) {
        current = start;
        prev = start;
        while (strcmp(ptr->filepathname, current->filepathname)) {
            prev = current;
            current = current->next;
        }
        if (current == start)
            start = current->next;
        else
            prev->next = current->next;
        free(current->filepathname);
        free(current);
        tmp = ptr;
        ptr = ptr->next;
        free(tmp);
    }
    conf->expanded = start;
    if (conf->afile)
        free(conf->afile);
    conf->afile = rpath;
}

/* Creates archives according to configuration. Relies on make_archive for the
 * actual archive creation process. This function mostly just handles the
 * "bookkeeping" in between archives.
 *
 * Progress will be stored in DEFAULT_RESUME, and can be resumed with the -r
 * option. If DEFAULT_RESUME already exists, you will be warned and need to
 * manually indicate that you wish to continue and overwrite past progress.
 *
 */
void
archiving(struct configuration *conf,
          bool upload,
          bool resume,
          const char *p,
          bool hash)
{
    FILE *fp;
    int res, rpsize, c, i, clen;
    struct stat buf;
    char *gpg_conf, *resume_path, *rname = NULL;
    const char errmsg[] = "cebac.c:archiving";
    char confirm[] = "YES";
    char buffer[20];
    char hash_buffer[100];
    char *hash_file = NULL;

    /* Here we will start by initializing the gpg engine. */
    gpgme_init();
    /* Now initialize libgcrypt. */
    init_gcrypt();
    /* Intiate Python. */
    init_python(p);

    rpsize = strlen(getenv("HOME"));
    rpsize += strlen(DEFAULT_RESUME);
    rpsize++;

    resume_path = malloc(rpsize);
    if (!resume_path)
        error_and_exit(errmsg, errno, NULL);
    strcpy(resume_path, getenv("HOME"));
    strcat(resume_path, DEFAULT_RESUME);

    gpg_conf = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_GPG_DIR) + 1);
    if (!gpg_conf)
        error_and_exit(errmsg, errno, NULL);
    strcpy(gpg_conf, getenv("HOME"));
    strcat(gpg_conf, DEFAULT_GPG_DIR);

    errno = 0;
    res = stat(resume_path, &buf);
    if (!resume && errno != ENOENT) {
        clen = strlen(confirm);
        i = 0;
        printf("Warning: resume progress exists from previous run.\n"
               "If you continue, then you will no longer be able to continue"
               " previous progress.\nType in YES to confirm that you wish to "
               "continue and overwrite previous progress\n");
        while ((c = fgetc(stdin)) != EOF && i < clen) {
            if (c != confirm[i++])
                exit(EXIT_SUCCESS);
        }
        remove(resume_path);
    }

    while (conf->expanded || conf->stage != first_archive) {
        if (conf->stage == first_archive) {
            make_archive(conf, resume);
            printf("Created archive: %s\n", conf->afile);
            if (hash) {
                get_sha256(conf->afile, hash_buffer, 100);
                /* We need to hash here is encryption is not enabled. */
                if (conf->encryption_method == unencrypted) {
                    if (!hash_file) {
                        rpsize = strlen(conf->archive_location) + 2;
                        rpsize += strlen(conf->archive_name) + 12;
                        if (!(hash_file = malloc(rpsize)))
                            error_and_exit(errmsg, errno, NULL);
                        strcpy(hash_file, conf->archive_location);
                        strcat(hash_file, FILEPATH_SEP);
                        strcat(hash_file, conf->archive_name);
                        strcat(hash_file, HASH_SUFFIX);
                    }
                    if (!(fp = fopen(hash_file, "a")))
                        error_and_exit(errmsg, errno, NULL);
                    res = fwrite(hash_buffer, sizeof(hash_buffer[0]),
                                 strlen(hash_buffer), fp);
                    if ((unsigned int)res != strlen(hash_buffer))
                        error_and_exit(errmsg, errno, NULL);
                    /* Now to find the archive name that was last processed. */
                    i = 0;
                    c = 0;
                    while (*((conf->afile) + i)) {
                        if (*((conf->afile) + i) == FILEPATH_SEP[0])
                            c = i;
                        i++;
                    }
                    c++;
                    res = fwrite("  ", 1, 2, fp);
                    if (res != 2)
                        error_and_exit(errmsg, errno, NULL);
                    res = fwrite(conf->afile + c, sizeof((conf->afile)[c]),
                                 strlen(conf->afile + c), fp);
                    if ((unsigned int)res != strlen(conf->afile + c))
                        error_and_exit(errmsg, errno, NULL);
                    res = fwrite("\n", 1, 1, fp);
                    if (res != 1)
                        error_and_exit(errmsg, errno, NULL);
                    res = fclose(fp);
                    if (res == EOF)
                        error_and_exit(errmsg, errno, NULL);
                }
            }
            if (conf->encryption_method != unencrypted)
                conf->stage = second_encrypt;
            else if (conf->encryption_method == unencrypted && upload)
                conf->stage = third_upload;
            write_progress(conf, resume_path);
        }
        if (conf->stage == second_encrypt) {
            if (conf->encryption_method == gpg_pass)
                encrypt_file(conf->afile,
                             NULL,
                             conf->gpg_password,
                             &rname,
                             gpg_conf,
                             resume);
            else if (conf->encryption_method == gpg_key)
                encrypt_file(conf->afile,
                             conf->gpg_recipient,
                             NULL,
                             &rname,
                             gpg_conf,
                             resume);
            remove(conf->afile);
            free(conf->afile);
            conf->afile = rname;
            if (upload)
                conf->stage = third_upload;
            printf("Created encrypted archive: %s\n", conf->afile);
            if (hash) {
                get_sha256(conf->afile, hash_buffer, 100);
                if (!hash_file) {
                    rpsize = strlen(conf->archive_location) + 2;
                    rpsize += strlen(conf->archive_name) + 12;
                    if (!(hash_file = malloc(rpsize)))
                        error_and_exit(errmsg, errno, NULL);
                    strcpy(hash_file, conf->archive_location);
                    strcat(hash_file, FILEPATH_SEP);
                    strcat(hash_file, conf->archive_name);
                    strcat(hash_file, HASH_SUFFIX);
                }
                if (!(fp = fopen(hash_file, "a")))
                    error_and_exit(errmsg, errno, NULL);
                res = fwrite(hash_buffer, sizeof(hash_buffer[0]),
                             strlen(hash_buffer), fp);
                if ((unsigned int)res != strlen(hash_buffer))
                    error_and_exit(errmsg, errno, NULL);
                /* Now to find the archive name that was last processed. */
                i = 0;
                c = 0;
                while (*((conf->afile) + i)) {
                    if (*((conf->afile) + i) == FILEPATH_SEP[0])
                        c = i;
                    i++;
                }
                c++;
                res = fwrite("  ", 1, 2, fp);
                if (res != 2)
                    error_and_exit(errmsg, errno, NULL);
                res = fwrite(conf->afile + c, sizeof((conf->afile)[c]),
                             strlen(conf->afile + c), fp);
                if ((unsigned int)res != strlen(conf->afile + c))
                    error_and_exit(errmsg, errno, NULL);
                res = fwrite("\n", 1, 1, fp);
                if (res != 1)
                    error_and_exit(errmsg, errno, NULL);
                res = fclose(fp);
                if (res == EOF)
                    error_and_exit(errmsg, errno, NULL);
            }
                write_progress(conf, resume_path);
        }
        if (conf->stage == third_upload) {
            switch (conf->upload_t) {
                case aws:
                    switch (conf->aws_type) {
                        case s3:
                            strncpy(buffer, "STANDARD", 19);
                            break;
                        case glacier:
                            strncpy(buffer, "GLACIER", 19);
                            break;
                        case deep_archive:
                            strncpy(buffer, "DEEP_ARCHIVE", 19);
                            break;
                    }
                    upload_to_aws(conf->afile, conf->aws_name, buffer);
                    printf("Archive: %s has been uploaded to aws: %s\n",
                           conf->afile, conf->aws_name);
                    break;
                case script:
                    run_custom_script(conf->afile, conf->script_path);
                    printf("Archive: %s has been handled by the custom "
                           "script \"%s\"\n", conf->afile, conf->script_path);
                    break;
                case dropbox:
                    upload_to_dropbox(conf->afile, conf->dropbox_token,
                                      conf->dropbox_path);
                    printf("Archive: %s has been uploaded to dropbox.\n",
                           conf->afile);
                    break;
            }
            res = remove(conf->afile);
            if (res == -1)
                error_and_exit(errmsg, errno, conf->afile);
            write_progress(conf, resume_path);
        }
        conf->stage = first_archive;
        /* Remove conf->afile. */
        free(conf->afile);
        conf->afile = NULL;
        (conf->archive_cur)++;
        write_progress(conf, resume_path);
    }
    /* Now we upload the hashes file, if hashing is enabled. */
    if (hash && upload) {
        /* First get hash file path if it does not exist. */
        if (!hash_file) {
            rpsize = strlen(conf->archive_location) + 2;
            rpsize += strlen(conf->archive_name) + 12;
            if (!(hash_file = malloc(rpsize)))
                error_and_exit(errmsg, errno, NULL);
            strcpy(hash_file, conf->archive_location);
            strcat(hash_file, FILEPATH_SEP);
            strcat(hash_file, conf->archive_name);
            strcat(hash_file, HASH_SUFFIX);
        }
        /* Now we check for the hash file. */
        res = stat(hash_file, &buf);
        /* If we get an error we will exit in failure. But, but the error is
         * caused by the file not existing, then we will ignore it. If we
         * reach this stage, but the hash file is missing then there is no
         * way to recover. */
        if (res == -1 && errno != ENOENT)
            error_and_exit(errmsg, errno, NULL);
        else if (!res) {
            switch (conf->upload_t) {
                case aws:
                    switch (conf->aws_type) {
                        case s3:
                            strncpy(buffer, "STANDARD", 19);
                            break;
                        case glacier:
                            strncpy(buffer, "GLACIER", 19);
                            break;
                        case deep_archive:
                            strncpy(buffer, "DEEP_ARCHIVE", 19);
                            break;
                    }
                    upload_to_aws(hash_file, conf->aws_name, buffer);
                    printf("Hash collection: %s has been uploaded to aws:"
                           "%s\n", hash_file, conf->aws_name);
                    break;
                case script:
                    run_custom_script(conf->afile, conf->script_path);
                    printf("Hash collection: %s has been handled by the custom"
                           " script \"%s\"\n", hash_file, conf->script_path);
                    break;
                case dropbox:
                    upload_to_dropbox(hash_file, conf->dropbox_token,
                                      conf->dropbox_path);
                    printf("Hash collection: %s has been uploaded "
                           "to dropbox.\n", hash_file);
                    break;
            }
        }
    }
    /* After everything has successfully completed, we delete the resume
     * files. */
    res = remove(resume_path);
    if (res == -1)
        error_and_exit(errmsg, errno, resume_path);
    free(resume_path);
    free(gpg_conf);
    resume_path = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_FILES) + 1);
    if (!resume_path)
        error_and_exit(errmsg, errno, NULL);
    strcpy(resume_path, getenv("HOME"));
    strcat(resume_path, DEFAULT_FILES);
    remove(resume_path);
    free(resume_path);
    if (hash_file)
        free(hash_file);

    destroy_python();
}

/* Writes everything in our struct configuration into the resume file, so if
 * we abort the process at any point, we can pick up where we were.
 *
 * Currently this function is a mess of fprintf and error handling.
 * Possibly look into this at some point for a better solution.
 */
static void
write_progress(struct configuration *conf, const char *rpath)
{
    FILE *fd;
    const char errmsg[] = "cebac.c:write_progress";
    struct stringlist *p;
    long long nfiles;
    int res;

    fd = fopen(rpath, "w");
    if (!fd)
        error_and_exit(errmsg, errno, rpath);
    res = fprintf(fd, "# DO NOT EDIT THIS FILE\n");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    res = fprintf(fd, "archive_type=");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    switch (conf->format_options.type_id.archive_type) {
        case tar:
            res = fprintf(fd, "\"tar\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case s7zip:
            res = fprintf(fd, "\"7zip\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "7zip_compression=\"%s\"\n",
                          conf->format_options.options[1]);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "7zip_compression-level=\"%s\"\n",
                          conf->format_options.options[3]);
            break;
    }
    res = fprintf(fd, "compression_format=");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    switch (conf->filter_options.type_id.compression_format) {
        case none:
            res = fprintf(fd, "\"none\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case xz:
            res = fprintf(fd, "\"xz\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "xz_compression-level=\"%s\"\n",
                          conf->filter_options.options[1]);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "xz_threads=\"%s\"\n",
                          conf->filter_options.options[3]);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case zstd:
            res = fprintf(fd, "\"zstd\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "zstd_compression-level=\"%s\"\n",
                          conf->filter_options.options[1]);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
    }
    res = fprintf(fd, "encryption_method=");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    switch (conf->encryption_method) {
        case unencrypted:
            res = fprintf(fd, "\"unencrypted\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case gpg_pass:
            res = fprintf(fd, "\"gpg_pass\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            if (conf->gpg_password) {
                res = fprintf(fd, "gpg_password=\"%s\"\n", conf->gpg_password);
                if (res < 1)
                    error_and_exit(errmsg, errno, rpath);
            }
            break;
        case gpg_key:
            res = fprintf(fd, "\"gpg_key\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            if (conf->gpg_recipient) {
                res = fprintf(fd, "gpg_recipient=\"%s\"\n",
                              conf->gpg_recipient);
                if (res < 1)
                    error_and_exit(errmsg, errno, rpath);
            }
            break;
    }
    if (conf->archive_name) {
        res = fprintf(fd, "archive_name=\"%s\"\n", conf->archive_name);
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
    }
    if (conf->archive_location) {
        res = fprintf(fd, "archive_location=\"%s\"\n", conf->archive_location);
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
    }
    res = fprintf(fd, "upload_type=");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    switch (conf->upload_t) {
        case aws:
            res = fprintf(fd, "\"aws\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "aws_type=");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            switch (conf->aws_type) {
                case s3:
                    res = fprintf(fd, "\"s3\"\n");
                    if (res < 1)
                        error_and_exit(errmsg, errno, rpath);
                    break;
                case glacier:
                    res = fprintf(fd, "\"glacier\"\n");
                    if (res < 1)
                        error_and_exit(errmsg, errno, rpath);
                    break;
                case deep_archive:
                    res = fprintf(fd, "\"deep_archive\"\n");
                    if (res < 1)
                        error_and_exit(errmsg, errno, rpath);
                    break;
            }
            if (conf->aws_name) {
                res = fprintf(fd, "aws_name=\"%s\"\n", conf->aws_name);
                if (res < 1)
                    error_and_exit(errmsg, errno, rpath);
            }
            break;
        case script:
            res = fprintf(fd, "\"custom\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "script_path=\"%s\"\n", conf->script_path);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case dropbox:
            res = fprintf(fd, "\"dropbox\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "dropbox_token=\"%s\"\n", conf->dropbox_token);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            res = fprintf(fd, "dropbox_path=\"%s\"\n", conf->dropbox_path);
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
    }
    p = conf->bfiles;
    res = fprintf(fd, "backup_files=");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    while (p) {
        res = fprintf(fd, "\"%s\"", p->filepathname);
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
        if (p->next) {
            res = fprintf(fd, ",");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
        } else {
            res = fprintf(fd, "\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
        }
        p = p->next;
    }
    p = conf->xfiles;
    if (p) {
        res = fprintf(fd, "exclude_files=");
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
    }
    while (p) {
        res = fprintf(fd, "\"%s\"", p->filepathname);
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
        if (p->next) {
            res = fprintf(fd, ",");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
        } else {
            res = fprintf(fd, "\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
        }
        p = p->next;
    }
    res = fprintf(fd, "max_size=\"%lld\"\n", conf->max_size);
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    res = fprintf(fd, "archive_cur=\"%d\"\n", conf->archive_cur);
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    if (conf->afile) {
        res = fprintf(fd, "afile=\"%s\"\n", conf->afile);
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
    }
    if (conf->expanded) {
        nfiles = write_files_binary(conf->expanded);
        res = fprintf(fd, "expanded=\"%lld\"\n", nfiles);
        if (res < 1)
            error_and_exit(errmsg, errno, rpath);
    }
    res = fprintf(fd, "stage=");
    if (res < 1)
        error_and_exit(errmsg, errno, rpath);
    switch (conf->stage) {
        case first_archive:
            res = fprintf(fd, "\"first_archive\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case second_encrypt:
            res = fprintf(fd, "\"second_encrypt\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
        case third_upload:
            res = fprintf(fd, "\"third_upload\"\n");
            if (res < 1)
                error_and_exit(errmsg, errno, rpath);
            break;
    }
    res = fclose(fd);
    if (res == EOF)
        error_and_exit(errmsg, errno, rpath);
}

/* This creates the default configuration dir, if it does not exist.
 */
void
create_default_conf_dir(void)
{
    char *cdir;
    struct stat buf;
    int res;
    FILE *fp;
    const char errmsg[] = "cebac.c:create_default_conf_dir";
    const char gpg_conf[] = "s2k-mode 3\n"
                            "s2k-count 65011712\n"
                            "s2k-cipher-algo AES256\n"
                            "s2k-digest-algo SHA512\n";

    /* Start by checking that ~/.config exists, and create if not. */
    cdir = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_HOMECONF) + 1);
    if (!cdir)
        error_and_exit(errmsg, errno, NULL);
    strcpy(cdir, getenv("HOME"));
    strcat(cdir, DEFAULT_HOMECONF);

    errno = 0;
    res = stat(cdir, &buf);
    if (res == -1 && errno != ENOENT)
        error_and_exit(errmsg, errno, cdir);
    else if (res == -1 && errno == ENOENT) {
        res = mkdir(cdir, 0700);
        if (res == -1)
            error_and_exit(errmsg, errno, cdir);
    } else if (!res && !S_ISDIR(buf.st_mode))
        error_and_exit(errmsg, errno, cdir);
    free(cdir);

    cdir = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_CONF_DIR) + 1);
    if (!cdir)
        error_and_exit(errmsg, errno, NULL);
    strcpy(cdir, getenv("HOME"));
    strcat(cdir, DEFAULT_CONF_DIR);

    errno = 0;
    res = stat(cdir, &buf);
    if (res == -1 && errno != ENOENT)
        error_and_exit(errmsg, errno, cdir);
    else if (res == -1 && errno == ENOENT) {
        res = mkdir(cdir, 0700);
        if (res == -1)
            error_and_exit(errmsg, errno, cdir);
    } else if (!res && !S_ISDIR(buf.st_mode))
        error_and_exit(errmsg, errno, cdir);
    free(cdir);

    cdir = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_GPG_DIR) + 1);
    if (!cdir)
        error_and_exit(errmsg, errno, NULL);
    strcpy(cdir, getenv("HOME"));
    strcat(cdir, DEFAULT_GPG_DIR);

    errno = 0;
    res = stat(cdir, &buf);
    if (res == -1 && errno != ENOENT)
        error_and_exit(errmsg, errno, cdir);
    else if (res == -1 && errno == ENOENT) {
        res = mkdir(cdir, 0700);
        if (res == -1)
            error_and_exit(errmsg, errno, cdir);
    }
    free(cdir);
    /* The create default gpg.conf to be used with our program. */
    cdir = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_GPG_CONF) + 1);
    if (!cdir)
        error_and_exit(errmsg, errno, NULL);
    strcpy(cdir, getenv("HOME"));
    strcat(cdir, DEFAULT_GPG_CONF);
    fp = fopen(cdir, "w");
    if (!fp)
        error_and_exit(errmsg, errno, cdir);
    fwrite(gpg_conf, strlen(gpg_conf), 1, fp);
    res = ferror(fp);
    if (res)
        error_and_exit(errmsg, errno, cdir);
    res = fclose(fp);
    if (res == EOF)
        error_and_exit(errmsg, errno, cdir);
    free(cdir);

}

/* Writes the conf.expanded member as binary data.
 *
 * Returns the number of records written.
 */
static long long
write_files_binary(struct files *flist)
{
    long long nfiles = 0;
    size_t flen, result;
    int res;
    struct files *ptr;
    char *outfile;
    FILE *fp;
    char errmsg[] = "cebac.c:write_files_binary";

    outfile = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_FILES) + 1);
    if (!outfile)
        error_and_exit(errmsg, errno, NULL);

    strcpy(outfile, getenv("HOME"));
    strcat(outfile, DEFAULT_FILES);

    fp = fopen(outfile, "w");
    if (!fp)
        error_and_exit(errmsg, errno, outfile);

    ptr = flist;
    while (ptr) {
        if (!ptr->filepathname)
            cerror_and_exit(errmsg, "filepathname is NULL.", NULL);
        flen = strlen(ptr->filepathname) + 1;
        result = fwrite(&flen, sizeof(size_t), 1, fp);
        if (result != 1)
            error_and_exit(errmsg, errno, outfile);
        result = fwrite(ptr->filepathname, flen, 1, fp);
        if (result != 1)
            error_and_exit(errmsg, errno, outfile);
        result = fwrite(&(ptr->fsize), sizeof(long long), 1, fp);
        if (result != 1)
            error_and_exit(errmsg, errno, outfile);
        nfiles++;
        ptr = ptr->next;
    }
    res = fclose(fp);
    if (res == EOF)
        error_and_exit(errmsg, errno, outfile);
    free(outfile);

    return nfiles;

}

/* Reads nfiles entries and stores it into dynamically allocated struct files.
 * It is up to the calling function to ensure that it is freed when
 * appropriate.
 *
 * Returns a pointer to the first element.
 */
static struct files *
read_files_binary(long long nfiles)
{
    long long i;
    size_t flen, result;
    struct files *start, *last, *tmp;
    char *infile, *fpath = NULL;
    FILE *fp;
    char errmsg[] = "cebac.c:read_files_binary";
    start = NULL;

    infile = malloc(strlen(getenv("HOME")) + strlen(DEFAULT_FILES) + 1);
    if (!infile)
        error_and_exit(errmsg, errno, NULL);

    strcpy(infile, getenv("HOME"));
    strcat(infile, DEFAULT_FILES);

    fp = fopen(infile, "r");
    if (!fp)
        error_and_exit(errmsg, errno, infile);

    for (i = 0; i < nfiles; i++) {
        result = fread(&flen, sizeof(size_t), 1, fp);
        if (result != 1)
            error_and_exit(errmsg, errno, infile);
        if (!(tmp = malloc(sizeof(struct files))) ||
            !(fpath = malloc(flen)))
            error_and_exit(errmsg, errno, NULL);
        tmp->next = NULL;
        tmp->filepathname = fpath;
        if (!start) {
            start = tmp;
            last = tmp;
        } else {
            last->next = tmp;
            last = tmp;
        }
        result = fread(tmp->filepathname, flen, 1, fp);
        if (result != 1)
            error_and_exit(errmsg, errno, infile);
        result = fread(&(tmp->fsize), sizeof(long long), 1, fp);
        if (result != 1)
            error_and_exit(errmsg, errno, infile);
    }
    fclose(fp);
    free(infile);

    return start;
}

/* Prints basic help information.*/
void
print_cebac_help(const char *launch_name)
{
    printf(
"\n\ncebac - Compress Encrypt, BACkup\n"
"Easily handle large amounts of data for cloud backup.\n"
"Check the example configuration file, or the cebac.conf.5 manpage for "
"details on configuration options available.\n\n"
"Default configuration file location: "
"%s%s\n"
"This file will not be created by cebac as there are no suitable default "
"options. Use the example configuration file as a starting point to create "
"your own.\n\n"
"Syntax: %s [options] [optional path]\n\n"
"Options:\n\n"
" -c FILEPATH\tuse configuration file at FILEPATH\n"
" -v\t\tprint version information and exit\n"
" -p\t\tprint configuration options\n"
" -f\t\tprint all files for backup\n"
" -a\t\tcreate archives\n"
" -u\t\tupload archives, requires -a\n"
" -r\t\tresume aborted operation\n"
" -s\t\tcalculate sha256 for all archives\n"
" -h\t\tprints this information and exits\n\n"
"Please consult either the readme or the man pages for more details.\n"
, getenv("HOME"), DEFAULT_CONF, launch_name);
}
