/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error_handling.h"

/* error_and_exit - prints an error message to stderr and then exit failure.
 * msg is a pointer to a string containing source file and function
 * example: cebac.c:read_files_binary
 * error is the errno value
 * opt is an optional additional error message that will be printed at the
 * end of the message.
 */
void
error_and_exit(const char *msg, int error, const char *opt)
{
    const char errm[] = "Error in ";
    if (opt)
        fprintf(stderr, "%s%s\n%s: %s\n", errm, msg, strerror(error), opt);
    else
        fprintf(stderr, "%s%s\n%s\n", errm, msg, strerror(error));
    exit(EXIT_FAILURE);
}

/* custom_error_and_exit - prints a custom error message and exit failure.
 * msg is a pointer to a string containing source file and function
 * example: cebac.c:read_files_binary
 * custom is the custom error message
 */
void
cerror_and_exit(const char *msg, const char *custom, const char *opt)
{
    const char errm[] = "Error in ";
    if (opt)
        fprintf(stderr, "%s%s\n%s%s\n", errm, msg, custom, opt);
    else
        fprintf(stderr, "%s%s\n%s\n", errm, msg, custom);
    exit(EXIT_FAILURE);
}
