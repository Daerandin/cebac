#ifndef CEBAC_H
#define CEBAC_H

#define YEAR "2021-2022"
#define DEFAULT_CONF "/.config/cebac/cebac.conf"
#define DEFAULT_RESUME "/.config/cebac/resume"
#define DEFAULT_CONF_DIR "/.config/cebac"
#define DEFAULT_GPG_CONF "/.config/cebac/gpg/gpg.conf"
#define DEFAULT_GPG_DIR "/.config/cebac/gpg"
#define DEFAULT_FILES "/.config/cebac/files"
#define DEFAULT_HOMECONF "/.config"

#include <config.h>
#include "fileparser.h"
#include "configuration.h"

void create_default_conf_dir(void);
void cversion(void);
void populate_configuration(struct configuration *, const char *, bool);
void archiving(struct configuration *, bool, bool, const char *, bool);
void print_cebac_help(const char *);

#endif /* CEBAC_H */
