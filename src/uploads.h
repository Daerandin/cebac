#ifndef UPLOADS_H
#define UPLOADS_H

void upload_to_aws(const char *, const char *, const char *);
void run_custom_script(const char *, const char *);
void upload_to_dropbox(const char *, const char *, const char *);
void init_python(const char *);
void destroy_python(void);

#endif /* UPLOADS_H */
