/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This source file handles hashing of files. This funcitonality can be
 * optionally enabled to keep a list of hashes for future reference. */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <sys/stat.h>
#include "error_handling.h"

static void handle_error_and_die(unsigned char *,
                                 unsigned char *,
                                 int,
                                 const char *,
                                 const char *,
                                 int);

/* This function initializes libgcrypt, which is required before using any
 * functions from this library. */
void
init_gcrypt(void)
{
    const char errmsg[] = "hashing.c:init_gcrypt";
    gcry_error_t err;

    gcry_check_version(NULL);
    err = gcry_control(GCRYCTL_SUSPEND_SECMEM_WARN);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpg_strerror(err), NULL);
    err = gcry_control(GCRYCTL_INIT_SECMEM, 16384, 0);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpg_strerror(err), NULL);
    err = gcry_control(GCRYCTL_RESUME_SECMEM_WARN);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpg_strerror(err), NULL);
    err = gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpg_strerror(err), NULL);
}

/* This function calculates the sha256 hash of the file pointed to by path.
 * The hexadecimal hash is stored in dest. The calling function has to ensure
 * that dest has enough space to contain the hash, minimum 65 bytes.
 * The length parameter is the size in bytes of dest. This function will check
 * this number to ensure there is enough space to write the hex hash.
 */
void
get_sha256(const char *path, char *dest, int length)
{
    int i, fd, algor = GCRY_MD_SHA256;
    const char errmsg[] = "hashing.c:get_sha256";
    ssize_t result;
    struct stat buf;
    unsigned int bsize;
    unsigned char *buffer, *digest;

    bsize = gcry_md_get_algo_dlen(algor);
    /* Check if length is at least (bsize * 2) + 1 */
    if ((unsigned int)length < ((bsize * 2) + 1))
        cerror_and_exit(errmsg, "dest is not large enough to contain the "
                                "hexadecimal value of the hash.", NULL);
    i = stat(path, &buf);
    if (i == -1)
        error_and_exit(errmsg, errno, NULL);
    if ((digest = malloc(bsize)) == NULL)
        handle_error_and_die(NULL, NULL, errno, errmsg, NULL, 0);
    if ((buffer = malloc(buf.st_size)) == NULL)
        handle_error_and_die(digest, NULL, errno, errmsg, NULL, 0);
    fd = open(path, O_RDONLY);
    if (fd == -1)
        handle_error_and_die(digest, buffer, errno, errmsg, NULL, 0);
    result = read(fd, buffer, buf.st_size);
    if (result != buf.st_size)
        handle_error_and_die(digest, buffer, errno, errmsg, NULL, fd);
    close(fd);

    /* Now we have read the contents of the file into buffer, and can begin
     * calculating the hash. */
    gcry_md_hash_buffer(algor, digest, buffer, buf.st_size);
    free(buffer);
    for (i = 0; (unsigned int)i < bsize; i++)
        snprintf(&dest[i*2], 3, "%02x", digest[i]);
    free(digest);
}

static void
handle_error_and_die(unsigned char *first,
                     unsigned char *second,
                     int error,
                     const char *death_fun,
                     const char *opt,
                     int fd)
{
    if (first)
        free(first);
    if (second)
        free(second);
    if (fd)
        close(fd);
    if (opt)
        cerror_and_exit(death_fun, opt, NULL);
    else
        error_and_exit(death_fun, error, NULL);
}
