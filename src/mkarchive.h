#ifndef MKARCHIVE_H
#define MKARCHIVE_H

#include <archive.h>
#include "fileparser.h"

void create_archive(struct stringlist *,
                    const char *,
                    char **,
                    bool,
                    int (*)(struct archive *),
                    int (*)(struct archive *),
                    const char *,
                    const char *,
                    const char *);

#endif /* MKARCHIVE_H */
