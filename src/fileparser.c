/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file has code to expand a simple path into a collection of all the
 * underlying subfiles.
 */

#include <stdbool.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error_handling.h"
#include "fileparser.h"

#define STRUCT_STRINGLIST 0
#define STRUCT_EXCLUDE_LIST 1

struct worklist {
    char *name;
    struct worklist *next;
    struct exclude_list *xlist;
};

struct exclude_list {
    char *name;
    struct exclude_list *next;
};

static void clear_worklist(struct worklist *);
static void clear_exclude_list(struct exclude_list *);
static int compare_path(const char *, char *, char **);
static void attach_xclude(const char *,
                         void *,
                         struct exclude_list **,
                         int);
static int comp_files_ascending(const void *, const void *);
static int comp_files_descending(const void *, const void *);
static struct files *ffrom_name(const char *, long long);

/* Clears a struct files, including the provided pointer. */
void
clear_files(struct files *ptr)
{
    struct files *tmp;

    while (ptr) {
        if (ptr->filepathname)
            free(ptr->filepathname);
        tmp = ptr;
        ptr = ptr->next;
        free(tmp);
    }
}

/* recursively_expand - takes two arguments
 * fileslist is a pointer to a struct stringlist, the function will recursively
 * walk through any directories and subdirectories and expand it fully into
 * to list each and every individual file.
 * exclude is a pointer to a struct stringlist which lists files/directories
 * that are to be excluded from the expanded filelist.
 *
 * Note that it expects provided struct stringlist arguments to be VALID,
 * meaning that the end of both links contains a NULL initialized next pointer.
 *
 * Will return a pointer to a dynamically allocated struct files. It is up to
 * the calling function to ensure the struct files is freed when appropriate.
 * The new struct files exists completely independently of the other structs.
 */
struct files *
recursively_expand(struct stringlist *filelist, struct stringlist *exclude)
{
    DIR *dirp;
    bool skip, tofiles, towlist;
    struct files *start = NULL;
    struct files *current = NULL;
    struct files *temp;
    struct stringlist *xiterator;
    struct dirent *dir;
    struct stat buffer;
    struct exclude_list *tmp;
    const char errmsg[] = "fileparser.c:recursively_expand";
    char *strp = NULL;
    int result;
    struct worklist *wlstart, *wlist, *ptr, *end;
    wlist = NULL;
    end = NULL;
    wlstart = NULL;

    /* Moving all filepaths from filelist into a worklist, since we are going
     * to need to modify it as we process it. */
    while (filelist) {
        skip = false;
        if (!(filelist->filepathname))
            cerror_and_exit(errmsg, "NULL pointer in filepathname.", NULL);

        /* Check if we should completely skip current filepath. */
        xiterator = exclude;
        while (xiterator) {
            if (!(xiterator->filepathname))
                cerror_and_exit(errmsg, "NULL pointer in filepathname.", NULL);
            if (!strcmp(filelist->filepathname, xiterator->filepathname)) {
                skip = true;
                break;
            }
            xiterator = xiterator->next;
        }

        if (!skip) {
            ptr = malloc(sizeof(struct worklist));
            if (!ptr)
                error_and_exit(errmsg, errno, NULL);
            ptr->next = NULL;
            ptr->xlist = NULL;
            if (end)
                end->next = ptr;
            if (!wlist) {
                wlist = ptr;
                wlstart = ptr;
            }
            end = ptr;
            ptr->name = malloc(strlen(filelist->filepathname) + 1);
            if (!ptr->name)
                error_and_exit(errmsg, errno, NULL);
            strcpy(ptr->name, filelist->filepathname);

            /* Check if we should attach an exclude list to the struct. */
            attach_xclude(ptr->name, exclude, &(ptr->xlist), STRUCT_STRINGLIST);
        }
        filelist = filelist->next;
        
    }

    /* We have created a worklist to walk through. */
    while (wlist) {
        result = lstat(wlist->name, &buffer); 
        if (result == -1) {
            /* We will intentionally ignore these errors as they are most
             * likely caused by the user providing incorrect filenames in the
             * configuration file. Or the user deleted a file/dir while the
             * program was running. In any case, it is currently a design
             * choice to make the program ignore any such errors. */
            wlist = wlist->next;
            continue;
        }
        if (S_ISDIR(buffer.st_mode)) {
            dirp = opendir(wlist->name);
            if (!dirp)
                error_and_exit(errmsg, errno, wlist->name);
            errno = 0;
            dir = readdir(dirp);
            /* Iterate through the entire directory. */
            while (dir) {
                /* Skip over '.' and '..', as well as anything that matches
                 * any exclude pattern. */
                skip = false;
                if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
                    skip = true;
                /* Now check if we should skip based on the exclude list. */
                if (!skip) {
                    tmp = wlist->xlist;
                    while (tmp) {
                        if (!strcmp(dir->d_name, tmp->name)) {
                            skip = true;
                            break;
                        }
                        tmp = tmp->next;
                    }
                }
                /* If skip is still false by this point, we check the current
                 * read file/directory. */
                towlist = false;
                tofiles = false;
                if (!skip) {
                    switch (dir->d_type) {
                        case DT_DIR:
                            towlist = true;
                            break;
                        case DT_LNK:
                            /* fall through */
                        case DT_REG:
                            tofiles = true;
                            break;
                        case DT_UNKNOWN:
                            /* In this case, we need to stat the file. */
                            strp = malloc(strlen(wlist->name) +
                                          strlen(dir->d_name) + 2);
                            if (!strp)
                                error_and_exit(errmsg, errno, NULL);
                            strcpy(strp, wlist->name);
                            strcat(strp, FILEPATH_SEP);
                            strcat(strp, dir->d_name);
                            result = lstat(strp, &buffer);
                            if (result == -1)
                                error_and_exit(errmsg, errno, strp);
                            free(strp);
                            strp = NULL;
                            if (S_ISDIR(buffer.st_mode))
                                towlist = true;
                            else if (S_ISLNK(buffer.st_mode) ||
                                     S_ISREG(buffer.st_mode))
                                tofiles = true;
                            break;
                    }
                    /* If we found a dir, then it is put into the worklist. */
                    if (towlist) {
                        ptr = malloc(sizeof(struct worklist));
                        if (!ptr)
                            error_and_exit(errmsg, errno, NULL);
                        end->next = ptr;
                        end = ptr;
                        ptr->next = NULL;
                        ptr->xlist = NULL;
                        ptr->name = malloc(strlen(wlist->name) +
                                           strlen(dir->d_name) + 2);
                        if (!(ptr->name))
                            error_and_exit(errmsg, errno, NULL);
                        strcpy(ptr->name, wlist->name);
                        strcat(ptr->name, FILEPATH_SEP);
                        strcat(ptr->name, dir->d_name);
                        attach_xclude(dir->d_name, wlist->xlist, &(ptr->xlist),
                                      STRUCT_EXCLUDE_LIST);
                    } else if (tofiles) {
                        strp = malloc(strlen(wlist->name) +
                                      strlen(dir->d_name) + 2);
                        if (!strp)
                            error_and_exit(errmsg, errno, NULL);
                        strcpy(strp, wlist->name);
                        strcat(strp, FILEPATH_SEP);
                        strcat(strp, dir->d_name);
                        temp = ffrom_name(strp, 0);
                        free(strp);
                        strp = NULL;
                        if (!temp)
                            error_and_exit(errmsg, errno, NULL);
                        if (!start)
                            start = temp;
                        else
                            current->next = temp;
                        current = temp;
                    }
                }
                dir = readdir(dirp);
            }
            closedir(dirp);
            /* Now that we have completed walking through the directory, we
             * should still add it to our list. */
            temp = ffrom_name(wlist->name, 0);
            if (!temp)
                error_and_exit(errmsg, errno, NULL);
            if (!start)
                start = temp;
            else
                current->next = temp;
            current = temp;
        } else if (S_ISLNK(buffer.st_mode) || S_ISREG(buffer.st_mode)) {
            temp = ffrom_name(wlist->name, buffer.st_size);
            if (!temp)
                error_and_exit(errmsg, errno, NULL);
            if (!start)
                start = temp;
            else
                current->next = temp;
            current = temp;
        }
        wlist = wlist->next;
    }
    clear_worklist(wlstart);
    return start;
}

/* Helper function to clear worklist. */
static void
clear_worklist(struct worklist *wl)
{
    struct worklist *prev;
    while (wl) {
        if (wl->name)
            free(wl->name);
        clear_exclude_list(wl->xlist);
        prev = wl;
        wl = wl->next;
        free(prev);
    }
}

/* This function will compare the two strings, which have to be filsystem
 * paths or files.
 * If there is no similarity, then the function returns -1
 * If the paths are exactly equal, it returns 0
 * If entire full is equal to a beginning part of comp, then the function
 * returns 1, and ptr will point to comp[strlen(full) + 1], provided that
 * comp[strlen(full)] is a path seperator (/).
 */
static int
compare_path(const char *full, char *comp, char **ptr)
{
    int i = -1;
    long maxread = (strlen(full) < strlen(comp)) ? strlen(full) : strlen(comp);

    while (++i < maxread) {
        if (full[i] != comp[i]) {
            return -1;
        }
    }

    if (strlen(full) == strlen(comp))
        return 0;
    else if (strlen(full) < strlen(comp) && comp[maxread] == FILEPATH_SEP[0]) {
        *ptr = &comp[maxread+1];
        return 1;
    } else
        return -1;
}

/* Works through the xlist struct and compares each element to the provided
 * path element. It will create a dynamically allocated list with all
 * approriate sub-files/directories that match the path.
 *
 * The list will be set the pointer pointed to by attach. It is up to the
 * calling function to ensure this list is freed when appropriate.
 *
 */
static void
attach_xclude(const char *path,
              void *xfiles,
              struct exclude_list **attach,
              int type)
{
    void *xiterator;
    struct exclude_list *start = NULL;
    struct exclude_list *ptr, *new;
    char *pp = NULL;
    char *filepathname = NULL;
    const char errmsg[] = "fileparses.c:attach_exclude";

    ptr = NULL;
    xiterator = xfiles;
    while (xiterator) {
        if (type == STRUCT_STRINGLIST)
            filepathname = ((struct stringlist *)xiterator)->filepathname;
        else if (type == STRUCT_EXCLUDE_LIST)
            filepathname = ((struct exclude_list *)xiterator)->name;
        else
            error_and_exit(errmsg, EINVAL, NULL);
        if (compare_path(path, filepathname, &pp) == 1) {
            new = malloc(sizeof(struct exclude_list));
            if (!new)
                error_and_exit(errmsg, errno, NULL);
            new->name = pp;
            new->next = NULL;
            if (!start)
                start = new;
            else
                ptr->next = new;
            ptr = new;
        }
        if (type == STRUCT_STRINGLIST)
            xiterator = ((struct stringlist *)xiterator)->next;
        else if (type == STRUCT_EXCLUDE_LIST)
            xiterator = ((struct exclude_list *)xiterator)->next;
    }
    *attach = start;
}

/* Clears an exclude list. */
static void
clear_exclude_list(struct exclude_list *xlist)
{
    struct exclude_list *ptr, *prev;
    ptr = xlist;
    while (ptr) {
        prev = ptr;
        ptr = ptr->next;
        free(prev);
    }

}

/* sort_files - sorts the filelist based in filesize
 * Takes two arguments:
 * list is the struct filelist that is to be sorted.
 * ascending is an int, if non-zero then the list will be sorted in ascending
 * order, if zero then the list will be sorted in descending order.
 *
 * Returns a pointer to the first element of the sorted list.
 */
struct files *
sort_files(struct files *list, bool ascending)
{
    struct files **farray, *fwork;
    long i, nfiles = 0;
    int (*func)(const void *, const void *);
    const char errmsg[] = "fileparser.c:sort_files";

    if (ascending)
        func = comp_files_ascending;
    else
        func = comp_files_descending;

    fwork = list;
    while (fwork) {
        nfiles++;
        fwork = fwork->next;
    }

    /* We will put all struct files elements into an array, so that we can
     * sort it using qsort. */
    farray = malloc(sizeof(struct files) * nfiles);
    if (!farray)
        error_and_exit(errmsg, errno, NULL);
    for (i = 0; i < nfiles; i++) {
        farray[i] = list;
        list = list->next;
    }

    qsort(farray, nfiles, sizeof(struct files *), func);

    /* Now we walk through the array, and create new links according to the
     * new order. */
    for (i = 0; i < nfiles; i++) {
        if (i < (nfiles-1))
            farray[i]->next = farray[i+1];
        else
            farray[i]->next = NULL;
    }
    fwork = farray[0];
    free(farray);
    return fwork;
}

static int
comp_files_ascending(const void *a1, const void *a2)
{
    long long val1 = (*((struct files **)a1))->fsize;
    long long val2 = (*((struct files **)a2))->fsize;

    if (val1 < val2)
        return -1;
    else if (val1 == val2)
        return 0;
    else
        return 1;
}

static int
comp_files_descending(const void *a1, const void *a2)
{
    long long val1 = (*((struct files **)a1))->fsize;
    long long val2 = (*((struct files **)a2))->fsize;

    if (val1 > val2)
        return -1;
    else if (val1 == val2)
        return 0;
    else
        return 1;
}

/* Clears an entire struct stringlist. */
void
clear_stringlist(struct stringlist *slist)
{
    struct stringlist *prev, *current;

    current = slist;
    while (current) {
        if (current->filepathname)
            free(current->filepathname);
        prev = current;
        current = current->next;
        free(prev);
    }
}

/* remove_duplicates - remove duplicate entries from a struct files list.
 * Takes an argument of struct files, and will iterate through it looking for
 * duplicates. Any duplicates will be removed.
 *
 * Will change the ordering of elements in any way.
 */
void
remove_duplicates(struct files *flist)
{
    struct files *start, *ptr, *iterator, *prev, *tmp;
    char *fname;
    long long csize;
    start = flist;
    ptr = start;
    while (ptr) {
        csize = ptr->fsize;
        fname = ptr->filepathname;
        iterator = ptr->next;
        prev = ptr;
        while (iterator && iterator->fsize == csize) {
            tmp = iterator;
            iterator = iterator->next;
            if (!strcmp(fname, tmp->filepathname)) {
                prev->next = iterator;
                free(tmp->filepathname);
                free(tmp);
            } else
                prev = tmp;
        }
        ptr = ptr->next;
    }
}

/* Allocates space for a struct files and fills out the information
 * based on the provided path in the argument.
 *
 * If the size argument is greater than zero, then it is used as file size.
 * In other cases, this function will lstat the path to get size.
 *
 * Does not perform any tests on path, except in the case where it needs to
 * lstat it.
 *
 * Returns a pointer to the new struct on success, or NULL on failure.
 */
static struct files *
ffrom_name(const char *path, long long size)
{
    struct files *ptr;
    struct stat buf;
    int res;
    long long fsize;

    if (size < 1) {
        res = lstat(path, &buf);
        if (res == -1)
            return NULL;
        fsize = buf.st_size;
    } else
        fsize = size;
    ptr = malloc(sizeof(struct files));
    if (!ptr)
        return NULL;
    ptr->filepathname = malloc(strlen(path) + 1);
    if (!(ptr->filepathname)) {
        free(ptr);
        return NULL;
    }
    strcpy(ptr->filepathname, path);
    ptr->fsize = fsize;
    ptr->next = NULL;

    return ptr;
}
