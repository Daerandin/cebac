#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <archive.h>
#include "fileparser.h"
#include "mkarchive.h"

#define TAR_SUFFIX ".tar"
#define XZ_SUFFIX ".xz"
#define ZSTD_SUFFIX ".zst"
#define S7Z_SUFFIX ".7z"

#define A_7Z_COMPRESSION "compression"
#define A_7Z_LEVEL "compression-level"
#define F_XZ_LEVEL A_7Z_LEVEL
#define F_XZ_THREADS "threads"
#define F_ZSTD_LEVEL A_7Z_LEVEL

enum sarchive {
    tar,
    s7zip,
};

enum compression {
    none,
    xz,
    zstd,
};

enum encryption {
    unencrypted,
    gpg_pass,
    gpg_key,
};

enum aws {
    s3,
    glacier,
    deep_archive,
};

enum process {
    first_archive,
    second_encrypt,
    third_upload,
};

enum upload_type {
    aws,
    script,
    dropbox,
};

union ac {
    enum sarchive archive_type;
    enum compression compression_format;
};

struct fdata {
    union ac type_id;
    const char *string_identifier;
    char *suffix;
    int (*set_func)(struct archive *);
    const char **options;
};

struct configuration {
    struct fdata format_options;
    struct fdata filter_options;
    enum encryption encryption_method;
    char *archive_name;
    char *archive_location;
    char *gpg_recipient;
    char *gpg_password;
    enum upload_type upload_t;
    char *script_path;
    enum aws aws_type;
    char *aws_name;
    char *dropbox_token;
    char *dropbox_path;
    struct stringlist *bfiles;
    struct stringlist *xfiles;
    struct files *expanded;
    long long max_size;
    int archive_cur;
    char *afile;
    enum process stage;

};

void free_configuration(struct configuration *);
void print_configuration(struct configuration *);
void print_files(struct configuration *);
void set_format(struct configuration *, const char *);
void set_filter(struct configuration *, const char *);
void set_format_option(struct configuration *, const char *, const char *, const char *);
void set_filter_option(struct configuration *, const char *, const char *, const char *);
void set_archive_options(struct configuration *, char **);

#endif /* CONFIGURATION_H */
