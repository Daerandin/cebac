#ifndef ENCRYPTION_H
#define ENCRYPTION_H

#include <stdbool.h>

void gpgme_init(void);
void encrypt_file(const char *,
                 const char *,
                 char *,
                 char **,
                 const char *,
                 bool);

#endif /* ENCRYPTION_H */
