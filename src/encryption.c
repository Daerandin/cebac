/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file contains the encryption logic, relying mostly on gpgme. Functions
 * here are called from cebac.c.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gpgme.h>
#include <sys/stat.h>
#include <unistd.h>
#include "encryption.h"
#include "error_handling.h"

static gpgme_error_t provide_pass(void *,
                                  const char *,
                                  const char *,
                                  int,
                                  int);

/* Initialize the gpg engine, required before any further use of gpgme. Here
 * we shall also verify that the required protocols are provided. */
void
gpgme_init(void)
{
    gpgme_error_t err;

    gpgme_check_version(NULL);
    err = gpgme_engine_check_version(GPGME_PROTOCOL_OPENPGP);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit("encryption.c:gpgme_init", gpgme_strerror(err), NULL);
}

/* encrypt_file - encrypts a file, takes 4 arguments.
 * plain is the path to the file that is to be encrypted.
 * key is the public key that will be used for encryption.
 * pass is the password that will be used for symmetric encryption.
 * enc is where the filename of the newly encrypted file will be put.
 *
 * Note that either key or pass must be null. This will determine if a public
 * key will be used, OR a password for symmetric encryption.
 *
 * Once the encryption has completed, the new filename will be written to enc
 * which will be dynamically allocated. It is up to the calling function to
 * free it when appropriate.
 */
void
encrypt_file(const char *plain,
             const char *key,
             char *pass,
             char **enc,
             const char *gpg_conf,
             bool overwrite)
{
    gpgme_ctx_t ctx;
    gpgme_error_t err;
    gpgme_data_t in, out;
    gpgme_key_t pubkey = NULL, keylist[2];
    gpgme_engine_info_t info;
    char *outfile;
    int fd, ofd, res;
    struct stat buf;
    gpgme_error_t (*func)(void *, const char *, const char *, int, int);
    const char errmsg[] = "encryption.c:encrypt_file";

    func = provide_pass;

    keylist[1] = NULL;
    outfile = malloc(strlen(plain) + strlen(".gpg") + 1);
    if (!outfile)
        error_and_exit(errmsg, errno, NULL);

    strcpy(outfile, plain);
    strcat(outfile, ".gpg");

    errno = 0;
    stat(outfile, &buf);
    if (errno != ENOENT && !overwrite)
        cerror_and_exit(errmsg, "outfile already exists: ", outfile);

    err = gpgme_new(&ctx);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpgme_strerror(err), NULL);

    if (key) {
        /* Encryption using public key. */
        err = gpgme_get_key(ctx, key, &pubkey, 0);
        if (err != GPG_ERR_NO_ERROR)
            cerror_and_exit(errmsg, gpgme_strerror(err), NULL);

        keylist[0] = pubkey;

    } else {
        info = gpgme_ctx_get_engine_info(ctx);
        while (info && info->protocol != GPGME_PROTOCOL_OPENPGP)
            info = info->next;
        err = gpgme_ctx_set_engine_info(ctx,
                                        info->protocol,
                                        info->file_name,
                                        gpg_conf);
        if (err != GPG_ERR_NO_ERROR)
            cerror_and_exit(errmsg, gpgme_strerror(err), NULL);
        /* If pass is specified, this means we need to specifically provide it
         * through custom methods. */
        if (pass) {
            err = gpgme_set_pinentry_mode(ctx, GPGME_PINENTRY_MODE_LOOPBACK);
            if (err != GPG_ERR_NO_ERROR)
                cerror_and_exit(errmsg, gpgme_strerror(err), NULL);
            gpgme_set_passphrase_cb(ctx, func, pass);
        }
    }
    fd = open(plain, O_RDONLY);
    if (!fd)
        error_and_exit(errmsg, errno, plain);
    err = gpgme_data_new_from_fd(&in, fd);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpgme_strerror(err), NULL);

    ofd = open(outfile, O_WRONLY|O_CREAT, 0644);
    if (!ofd)
        error_and_exit(errmsg, errno, outfile);

    err = gpgme_data_new_from_fd(&out, ofd);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpgme_strerror(err), NULL);

    if (key)
        err = gpgme_op_encrypt(ctx, keylist, GPGME_ENCRYPT_ALWAYS_TRUST, in, out);
    else
        err = gpgme_op_encrypt(ctx, NULL, GPGME_ENCRYPT_ALWAYS_TRUST, in, out);
    if (err != GPG_ERR_NO_ERROR)
        cerror_and_exit(errmsg, gpgme_strerror(err), NULL);

    *enc = outfile;
    if (key)
        gpgme_key_unref(pubkey);
    gpgme_data_release(in);
    gpgme_data_release(out);
    res = close(fd);
    if (res == -1)
        error_and_exit(errmsg, errno, plain);
    res = close(ofd);
    if (res == -1)
        error_and_exit(errmsg, errno, outfile);
    gpgme_release(ctx);

}

static gpgme_error_t
provide_pass(void *pass,
             const char *ignore2,
             const char *ignore3,
             int ignore4,
             int dest)
{
    ssize_t res;
    char *outstring;
    const char errmsg[] = "encryption.c:provide_pass";
    outstring = malloc(strlen(pass) + 2);
    if (!outstring)
        error_and_exit(errmsg, errno, NULL);
    strcpy(outstring, pass);
    strcat(outstring, "\n");

    /* In order to avoid compiler warnings, we will ignore though all the
     * arguments. */
    if (ignore2){};
    if (ignore3){};
    if (ignore4){};

    res = write(dest, outstring, strlen(outstring));
    if (res == -1)
        error_and_exit(errmsg, errno, NULL);
    free(outstring);
    return 0;

}
