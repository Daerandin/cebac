#ifndef STRINGLINES_H
#define STRINGLINES_H

struct slines {
    char *line;
    int line_number;
    struct slines *next;
};

struct valuelist {
    char *value;
    struct valuelist *next;
};

struct vlines {
    char *variable;
    struct valuelist *values;
    int line_number;
    struct vlines *next;
};

struct slines *string_to_lines_destructive(char *);
struct vlines *extract_variable_value(struct slines *);
void clear_vlines(struct vlines *);

#endif /* STRINGLINES_H */
