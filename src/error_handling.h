#ifndef ERROR_HANDLING_H
#define ERROR_HANDLING_H

void error_and_exit(const char *, int, const char *);
void cerror_and_exit(const char *, const char *, const char *);

#endif /* ERROR_HANDLING_H */
