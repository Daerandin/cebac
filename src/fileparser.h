#ifndef FILEPARSER_H
#define FILEPARSER_H

#include <stdbool.h>

#define FILEPATH_SEP "/"

struct files {
    char *filepathname;
    long long fsize;
    struct files *next;
};

struct stringlist {
    char *filepathname;
    struct stringlist *next;
};

struct files *recursively_expand(struct stringlist *, struct stringlist *);
void clear_files(struct files *);
void clear_stringlist(struct stringlist *);
struct files *sort_files(struct files *, bool);
void remove_duplicates(struct files *);

#endif /* FILEPARSER_H */
