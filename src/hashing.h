#ifndef HASHING_H
#define HASHING_H

void init_gcrypt(void);
void get_sha256(const char *, char *, int);

#endif /* HASHING_H */
