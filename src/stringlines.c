/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file contains functions specifically designed to manage strings with
 * multiple lines, and split it into more manageable parts.
 */

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error_handling.h"
#include "stringlines.h"

enum read_mode {
    beginning,
    variable,
    delimiter,
    value,
    comment,
};

/* string_to_lines_destructive
 * This function takes a line, replaces every newline with a '\0' and then
 * puts pointers to the beginning of each line in a struct slines.
 *
 * The struct slines will be dynamically allocated, and it is up to the
 * calling function to ensure that it is freed when no longer useful.
 *
 * Returns a pointer to the struct slines.
 */
struct slines *
string_to_lines_destructive(char *str)
{
    const char errmsg[] = "stringlines.c:string_to_lines_destructive";
    struct slines *newlines, *current;
    /* You are not seriously calling this function with a NULL
     * pointer, right? */
    if (!str)
        cerror_and_exit(errmsg, "Required argument is NULL.", NULL);

    newlines = malloc(sizeof(struct slines));
    if (!newlines)
        error_and_exit(errmsg, errno, NULL);
    
    newlines->line = str;
    newlines->line_number = 1;
    newlines->next = NULL;

    current = newlines;

    while (*str) {
        if (*str == '\n') {
            *str = '\0';
            if (*(str + 1)) {
                current->next = malloc(sizeof(struct slines));
                if (!current->next)
                    error_and_exit(errmsg, errno, NULL);
                current->next->line = str + 1;
                current->next->line_number = current->line_number + 1;
                current->next->next = NULL;
                current = current->next;
            }
        }
        str++;
    }
    
    return newlines;
}

/* extract_variable_value
 * This function will iterate through the lines in the provided struct slines.
 * It will specifically look for patterns of the following type:
 * variable = "value"
 * or
 * variable = 'value'
 *
 * Spaces and tabs before and after the variable and '=' is ignored.
 * The value must be enclosed in either a pair of single quotes, or a pair of
 * double quotes.
 *
 * Multiple values can exist, seperated by a comma ',':
 * variable = "value1",'value2','value3',"value4"
 *
 * Returns a pointer to a dynamically allocated struct vlines. It is up to
 * the calling function to free the associated memory when appropriate.
 */
struct vlines *
extract_variable_value(struct slines *lines)
{
    struct vlines *start = NULL;
    struct vlines *current = NULL;
    struct vlines *work = NULL;
    struct valuelist *tmp, *ptr = NULL;
    char *iterator;
    char quote = '\0';
    enum read_mode cmode = beginning;
    char buffer[500], errbuff[4];
    int i = 0;
    const char errmsg[] = "stringlines.c:extract_variable_value";

    while (lines) {
        iterator = lines->line;
        while (*iterator) {
            switch (cmode) {
                case beginning:
                    /* Skip whitespace. */
                    while (isspace((unsigned char)*iterator))
                        iterator++;
                    cmode = variable;
                    break;
                case variable:
                    if (*iterator == '#') {
                        cmode = comment;
                        break;
                    }

                    while ((isalnum((unsigned char)*iterator) ||
                            *iterator == '_' || *iterator == '-') && i < 500)
                        buffer[i++] = *(iterator++);

                    if (i == 500)
                        cerror_and_exit(errmsg, "Variable larger than "
                                        "available buffer.", NULL);
                    buffer[i] = '\0';
                    i = 0;

                    work = malloc(sizeof(struct vlines));
                    if (!work)
                        error_and_exit(errmsg, errno, NULL);
                    work->line_number = lines->line_number;
                    work->values = NULL;
                    work->variable = NULL;
                    work->next = NULL;

                    if (!start)
                        start = work;
                    else
                        current->next = work;
                    current = work;

                    current->variable = malloc(strlen(buffer) + 1);
                    if (!current->variable)
                        error_and_exit(errmsg, errno, NULL);

                    strcpy(current->variable, buffer);

                    cmode = delimiter;
                    break;
                case delimiter:
                    /* Skip whitespace. */
                    while (isspace((unsigned char)*iterator))
                        iterator++;
                    if (*(iterator++) != '=') {
                        if (lines->line_number > 999)
                            cerror_and_exit(errmsg, "Missing delimiter '=' "
                                            "in configuration file.", NULL);
                        snprintf(errbuff, 4, "%d", lines->line_number);
                        cerror_and_exit(errmsg, "Missing delimiter '=' in "
                                        "configuration file, line number: ",
                                        errbuff);
                    }
                    while (isspace((unsigned char)*iterator))
                        iterator++;
                    /* At this point we are clearly past the delimiter
                     * and any whitespace before and/or after. */
                    cmode = value;
                    break;
                case value:
                    while (true) {
                        if (*iterator == '\'' || *iterator == '"')
                            quote = *(iterator++);
                        else {
                            if (lines->line_number > 999)
                                cerror_and_exit(errmsg, "Missing value start "
                                                "quote in configuration file."
                                                ,NULL);
                            snprintf(errbuff, 4, "%d", lines->line_number);
                            cerror_and_exit(errmsg, "Missing value start "
                                            "quote in configuration file "
                                            "line number: ", errbuff);
                        }

                        while (*iterator != quote && *iterator && i < 500)
                            buffer[i++] = *(iterator++);

                        if (i == 500 || !(*iterator)) {
                            if (lines->line_number > 999)
                                cerror_and_exit(errmsg, "No closing quote in "
                                                "configuration file.", NULL);
                            snprintf(errbuff, 4, "%d", lines->line_number);
                            cerror_and_exit(errmsg, "No closing quite in "
                                            "configuration file line: ",
                                            errbuff);
                        }
                        buffer[i] = '\0';
                        i = 0;

                        tmp = malloc(sizeof(struct valuelist));
                        if (!tmp)
                            error_and_exit(errmsg, errno, NULL);

                        tmp->value = NULL;
                        tmp->next = NULL;

                        tmp->value = malloc(strlen(buffer) + 1);
                        if (!(tmp->value))
                            error_and_exit(errmsg, errno, NULL);
                        strcpy(tmp->value, buffer);
                        if (!(current->values))
                                current->values = tmp;
                        else {
                            ptr = current->values;
                            while (ptr->next)
                                ptr = ptr->next;
                            ptr->next = tmp;
                        }
                        if (*(++iterator) != ',') {
                            cmode = beginning;
                            break;
                        } else if (!(*iterator))
                            break;
                        else
                            iterator++;
                    }
                    break;
                default:
                    if (*iterator)
                        iterator++;
            }
        }
        cmode = beginning;
        lines = lines->next;
    }

    return start;
}

/* Clear all the dynamically allocated memory associated with
 * a struct vlines, including the entire vlines link itself.
 */
void
clear_vlines(struct vlines *vl)
{
    struct vlines *previous;
    struct valuelist *ptr, *ptrn;

    while (vl) {
        previous = vl;
        if (vl->variable)
            free(vl->variable);

        ptrn = vl->values;
        while (ptrn) {
            ptr = ptrn;
            if (ptrn->value)
                free(ptrn->value);
            ptrn = ptrn->next;
            free(ptr);
        }

        vl = vl->next;
        free(previous);
    }
}
