/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "cebac.h"
#include "configuration.h"
#include "error_handling.h"

struct options {
    bool version;
    char *configuration_file;
    bool malloced_conf;
    bool print_configuration;
    bool print_files;
    bool make_archive;
    bool upload;
    bool resume;
    bool hash;
    bool help;
};

/* The main function will only handle argument parsing, and calling additional
 * functions from cebac.h, which is where the logic will be handled.
 */
int
main(int argc, char* argv[])
{
    uid_t userid;
    int opt;
    struct options cargs;
    struct configuration conf;
    cargs.version = false;
    cargs.configuration_file = NULL;
    cargs.print_configuration = false;
    cargs.print_files = false;
    cargs.make_archive = false;
    cargs.upload = false;
    cargs.resume = false;
    cargs.malloced_conf = true;
    cargs.hash = false;
    cargs.help = false;

    /* First check if program is run as root and terminate execution in that
     * case. This software is not written with security in mind, and is also
     * making use of the 'system' library function. So do not permit running
     * as root.
     */
    userid = geteuid();
    if (!userid) {
        fprintf(stderr, "Error: running as root is not supported.\n");
        return EXIT_FAILURE;
    }

    while ((opt = getopt(argc, argv, "c:vpfaruhs")) != -1) {
        switch (opt) {
            case 'c':
                cargs.configuration_file = optarg;
                cargs.malloced_conf = false;
                break;
            case 'v':
                cargs.version = true;
                break;
            case 'p':
                cargs.print_configuration = true;
                break;
            case 'f':
                cargs.print_files = true;
                break;
            case 'a':
                cargs.make_archive = true;
                break;
            case 'u':
                cargs.upload = true;
                break;
            case 'r':
                cargs.resume = true;
                break;
            case 'h':
                cargs.help = true;
                break;
            case 's':
                cargs.hash = true;
                break;
            default: /* '?' */
                fprintf(stderr,
                        "Usage: %s [-v]\n",
                        argv[0]);
                return EXIT_FAILURE;
        }
    }
    /* If we have the 'u' option defined, but not 'a', then we abort. */
    if (cargs.upload && !cargs.make_archive) {
        fprintf(stderr, "Error: The 'u' option requires 'a' option.\n");
        return EXIT_FAILURE;
    }

    /* If the default config dir does not exist, we create it now. */
    create_default_conf_dir();

    /* If user requested version information or help, that's all we do. */
    if (cargs.version || cargs.help) {
        cversion();
        if (cargs.help)
            print_cebac_help(argv[0]);
    } else {
        /* If -r is set, we will use the resume file as configuration. */
        if (cargs.resume) {
            cargs.configuration_file = malloc(strlen(getenv("HOME")) +
                        strlen(DEFAULT_RESUME) + 1);
            if (!cargs.configuration_file)
                error_and_exit("main.c:main", errno, NULL);
            strcpy(cargs.configuration_file, getenv("HOME"));
            strcat(cargs.configuration_file, DEFAULT_RESUME);
        }
        /* Load the default configuration file path, unless set. */
        if (!cargs.configuration_file) {
            cargs.configuration_file = malloc(strlen(getenv("HOME")) +
                        strlen(DEFAULT_CONF) + 1);
            if (!cargs.configuration_file)
                error_and_exit("main.c:main", errno, NULL);
            strcpy(cargs.configuration_file, getenv("HOME"));
            strcat(cargs.configuration_file, DEFAULT_CONF);
        }

        /* Initialize the conf struct. */
        conf.format_options.type_id.archive_type = -1;
        conf.format_options.string_identifier = NULL;
        conf.format_options.suffix = NULL;
        conf.format_options.set_func = NULL;
        conf.format_options.options = NULL;
        conf.filter_options.type_id.compression_format = -1;
        conf.filter_options.string_identifier = NULL;
        conf.filter_options.suffix = NULL;
        conf.filter_options.set_func = NULL;
        conf.filter_options.options = NULL;
        conf.encryption_method = -1;
        conf.gpg_recipient = NULL;
        conf.gpg_password = NULL;
        conf.upload_t = -1;
        conf.aws_type = -1;
        conf.aws_name = NULL;
        conf.script_path = NULL;
        conf.dropbox_token = NULL;
        conf.dropbox_path = NULL;
        conf.bfiles = NULL;
        conf.xfiles = NULL;
        conf.max_size = -1;
        conf.expanded = NULL;
        conf.archive_name = NULL;
        conf.archive_cur = 1;
        conf.archive_location = NULL;
        conf.afile = NULL;
        conf.stage = first_archive;

        populate_configuration(&conf, cargs.configuration_file, cargs.resume);

        /* Free configuration file if it has been malloced. */
        if (cargs.malloced_conf)
            free(cargs.configuration_file);

        if (cargs.print_configuration)
            print_configuration(&conf);

        if (cargs.print_files)
            print_files(&conf);

        if (cargs.make_archive)
            archiving(&conf, cargs.upload, cargs.resume, argv[0], cargs.hash);

        free_configuration(&conf);
        
    }

    return EXIT_SUCCESS;
}
