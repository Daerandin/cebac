/****************************************************************************
*                                                                           *
*  cebac - Copmress Encrypt BACkup selected files, upload to cloud storage  *
*  Copyright (C) 2021-2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This source file has code dealing with the actual configuration structure
 * used by the rest of the program.
 */

#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "configuration.h"
#include "error_handling.h"

static void verify_and_set_format(enum sarchive, const char **, const char *);
static void verify_and_set_filter(enum compression, const char **, const char *);

const char *format_string[] = {
                               "tar",
                               "7zip",
                             };

const char *filter_string[] = {
                               "none",
                               "xz",
                               "zstd",
                             };

const char *a_7z_compression[] = {
                                  "store",
                                  "deflate",
                                  "bzip2",
                                  "lzma1",
                                  "lzma2",
                                  "ppmd",
                                };
const char *the_num[] = {
                            "0",
                            "1",
                            "2",
                            "3",
                            "4",
                            "5",
                            "6",
                            "7",
                            "8",
                            "9",
                           "10",
                           "11",
                           "12",
                           "13",
                           "14",
                           "15",
                           "16",
                           "17",
                           "18",
                           "19",
                           "20",
                           "21",
                           "22",
                           "23",
                           "24",
                           "25",
                           "26",
                           "27",
                           "28",
                           "29",
                           "30",
                           "31",
                           "32",
                           "33",
                           "34",
                           "34",
                           "36",
                           "36",
                           "38",
                           "39",
                          };



/* print_files - prints out every single file that could be found in the
 * backup_files variable. Useful for debuggging purposes in regards to the
 * congiration file, or to ensure that all expected files are included.
 */
void
print_files(struct configuration *conf)
{
    struct files *ptr;
    char pref[] = " KMG";
    double num;
    int i;
    if (conf && conf->expanded) {
        printf("Printing all files that are to be included in the backup.\n"
               "Sorted according to filesize in descending order:\n");
        ptr = conf->expanded;
        while (ptr) {
            num = ptr->fsize;
            i = 0;
            while (num > 1024 && i < 4) {
                num /= (float)1024;
                i++;
            }
            printf("  %s - size: %.1f ", ptr->filepathname, num);
            if (i > 0)
                printf("%ci", pref[i]);
            printf("B\n");
            ptr = ptr->next;
        }
    }
}

/* print_configuration - prints out all the options specified in the provided
 * struct configuration. Useful for debugging purposes to ensure that the
 * configuration file is correct.
 */
void
print_configuration(struct configuration *conf)
{
    struct stringlist *ptr;
    char pref[] = " KMG";
    const char **ost;
    int i;
    double num;
    if (conf) {
        printf("Printing configuration options read from configuration file."
                "\n\nArchive type: ");
        if (conf->format_options.string_identifier) {
            printf("%s", conf->format_options.string_identifier);
            if (conf->format_options.options) {
                printf(" with additional options:\n");
                ost = conf->format_options.options;
                i = 0;
                while (ost[i]) {
                    printf("  %s: %s\n", ost[i], ost[i+1]);
                    i += 2;
                }
            } else
                printf("\n");
        }
        if (conf->archive_name)
            printf("Archive name: %s\n", conf->archive_name);
        if (conf->archive_location)
            printf("Archive storage location: %s\n", conf->archive_location);
        printf("Compression format: ");
        if (conf->filter_options.string_identifier) {
            printf("%s", conf->filter_options.string_identifier);
            if (conf->filter_options.options) {
                printf(" with additional options:\n");
                ost = conf->filter_options.options;
                i = 0;
                while (ost[i]) {
                    printf("  %s: %s\n", ost[i], ost[i+1]);
                    i += 2;
                }
            } else
                printf("\n");
        }
        printf("Encryption method: ");
        switch (conf->encryption_method) {
            case unencrypted:
                printf("unencrypted\n");
                break;
            case gpg_pass:
                printf("gpg_pass\n");
                if (conf->gpg_password)
                    printf("GPG password found with length: %zu\n",
                        strlen(conf->gpg_password));
                break;
            case gpg_key:
                printf("gpg_key\n");
                if (conf->gpg_recipient)
                    printf("GPG recipient key: %s\n", conf->gpg_recipient);
                break;
            default:
                printf("INVALID\n");
        }
        printf("Upload to: ");
        switch (conf->upload_t) {
            case aws:
                printf("aws with storage type: ");
                switch (conf->aws_type) {
                    case s3:
                        printf("s3\n");
                        break;
                    case glacier:
                        printf("glacier\n");
                        break;
                    case deep_archive:
                        printf("deep_archive\n");
                        break;
                    default:
                        printf("INVALID\n");
                }
                printf("AWS storage name: %s\n", conf->aws_name);
                break;
            case script:
                printf("custom script: %s\n", conf->script_path);
                break;
            case dropbox:
                printf("dropbox\nDropbox access token: %s\n", conf->dropbox_token);
                printf("Dropbox path: %s\n", conf->dropbox_path);
                break;
            default:
                printf("INVALID\n");
        }
        ptr = conf->bfiles;
        if (ptr) {
            printf("Backup files:\n");
            while (ptr) {
                printf("  %s\n", ptr->filepathname);
                ptr = ptr->next;
            }
        }
        ptr = conf->xfiles;
        if (ptr) {
            printf("Exclude files:\n");
            while (ptr) {
                printf("  %s\n", ptr->filepathname);
                ptr = ptr->next;
            }
        }
        if (conf->max_size > 0) {
            num = conf->max_size;
            i = 0;
            while (num > 1024 && i < 4) {
                num /= (float)1024;
                i++;
            }
            printf("Max size: %.1f ", num);
            if (i > 0)
                printf("%ci", pref[i]);
            printf("B\n");
        }
    }
}

/* free_configuration - takes a struct configuration and frees the memory
 * pointed to by all struct members that are pointers.
 *
 * If a pointer has been previously freed through other means, then it is
 * very important that the pointer is set to NULL. If not, this function
 * will lead to undefined behaviour.
 *
 * Just the same, the bfiles member is expected to either be NULL, or point
 * to a valid struct stringlist. And for valid struct stringlist, the next
 * pointer must either point to a valid struct files as well, or be NULL.
 */
void
free_configuration(struct configuration *conf)
{
    if (conf) {
        if (conf->gpg_recipient) {
            free(conf->gpg_recipient);
            conf->gpg_recipient = NULL;
        }
        if (conf->gpg_password) {
            free(conf->gpg_password);
            conf->gpg_password = NULL;
        }
        if (conf->aws_name) {
            free(conf->aws_name);
            conf->aws_name = NULL;
        }
        if (conf->script_path) {
            free(conf->script_path);
            conf->script_path = NULL;
        }
        if (conf->bfiles) {
            clear_stringlist(conf->bfiles);
            conf->bfiles = NULL;
        }
        if (conf->xfiles) {
            clear_stringlist(conf->xfiles);
            conf->xfiles = NULL;
        }
        if (conf->expanded) {
            clear_files(conf->expanded);
            conf->expanded = NULL;
        }
        if (conf->archive_name) {
            free(conf->archive_name);
            conf->archive_name = NULL;
        }
        if (conf->archive_location) {
            free(conf->archive_location);
            conf->archive_location = NULL;
        }
        if (conf->afile) {
            free(conf->afile);
            conf->afile = NULL;
        }
        if (conf->format_options.options) {
            free(conf->format_options.options);
            conf->format_options.options = NULL;
        }
        if (conf->filter_options.options) {
            free(conf->filter_options.options);
            conf->filter_options.options = NULL;
        }
        if (conf->dropbox_token) {
            free(conf->dropbox_token);
            conf->dropbox_token = NULL;
        }
        if (conf->dropbox_path) {
            free(conf->dropbox_path);
            conf->dropbox_path = NULL;
        }
    }
}

/* set_format - initializes the format_options member of a configuration.
 *
 * First argument must be a pointer to a struct configuration that will be
 * initialized.
 *
 * Second argument must be a string indicating one of the supported archive
 * formats.
 *
 * If the configuration already has a valid format specified, this will cause
 * an error.
 */
void
set_format(struct configuration *conf, const char *format)
{
    const char errmsg[] = "configuration.c:set_format";
    int i, slen;
    slen = sizeof(format_string) / sizeof(format_string[0]);

    if (!conf)
        cerror_and_exit(errmsg, "No struct configuration given.", NULL);
    if ((int)conf->format_options.type_id.archive_type != -1)
        cerror_and_exit(errmsg, "Attempting to set archive format, but it is "
                        "already initialized.", NULL);

    for (i = 0; i < slen; i++) {
        if (!strcmp(format, format_string[i]))
            break;
    }
    if (i == slen)
        cerror_and_exit(errmsg, "Unrecognized archive format: ", format);

    switch (i) {
        case 0: /* tar */
            conf->format_options.type_id.archive_type = tar;
            conf->format_options.string_identifier = format_string[i];
            conf->format_options.suffix = TAR_SUFFIX;
            conf->format_options.set_func = archive_write_set_format_pax;
            break;
        case 1: /* 7zip */
            conf->format_options.type_id.archive_type = s7zip;
            conf->format_options.string_identifier = format_string[i];
            conf->format_options.suffix = S7Z_SUFFIX;
            conf->format_options.set_func = archive_write_set_format_7zip;
            conf->format_options.options = malloc((sizeof(char **) * 3*2)-1);
            if (!conf->format_options.options)
                error_and_exit(errmsg, errno, NULL);
            conf->format_options.options[0] = A_7Z_COMPRESSION;
            conf->format_options.options[1] = a_7z_compression[3];
            conf->format_options.options[2] = A_7Z_LEVEL;
            conf->format_options.options[3] = the_num[6];
            conf->format_options.options[4] = NULL;
            break;
    }
}

/* set_filter - initializes the filter_options member of a configuration.
 *
 * First argument must be a pointer to a struct configuration that will be
 * initialized.
 *
 * Second argument must be a string indicating one of the supported
 * compression filters.
 *
 * If the configuration already has a valid filter specified, this will cause
 * an error.
 */
void
set_filter(struct configuration *conf, const char *filter)
{
    const char errmsg[] = "configuration.h:set_filter";
    int i, slen;
    slen = sizeof(filter_string) / sizeof(filter_string[0]);

    if (!conf)
        cerror_and_exit(errmsg, "No struct configuration given.", NULL);
    if ((int)conf->filter_options.type_id.compression_format != -1)
        cerror_and_exit(errmsg, "Attempted to set compression filter, but it "
                                "is already initilized.", NULL);

    for (i = 0; i < slen; i++) {
        if (!strcmp(filter, filter_string[i]))
            break;
    }
    if (i == slen)
        cerror_and_exit(errmsg, "Unrecognized compression filter: ", filter);

    switch (i) {
        case 0: /* none */
            conf->filter_options.type_id.compression_format = none;
            conf->filter_options.string_identifier = filter_string[i];
            break;
        case 1: /* xz */
            conf->filter_options.type_id.compression_format = xz;
            conf->filter_options.string_identifier = filter_string[i];
            conf->filter_options.suffix = XZ_SUFFIX;
            conf->filter_options.set_func = archive_write_add_filter_xz;
            conf->filter_options.options = malloc((sizeof(char **) * 3*2)-1);
            if (!conf->filter_options.options)
                error_and_exit(errmsg, errno, NULL);
            conf->filter_options.options[0] = F_XZ_LEVEL;
            conf->filter_options.options[1] = the_num[6];
            conf->filter_options.options[2] = F_XZ_THREADS;
            conf->filter_options.options[3] = the_num[1];
            conf->filter_options.options[4] = NULL;
            break;
        case 2: /* zstd */
            conf->filter_options.type_id.compression_format = zstd;
            conf->filter_options.string_identifier = filter_string[i];
            conf->filter_options.suffix = ZSTD_SUFFIX;
            conf->filter_options.set_func = archive_write_add_filter_zstd;
            conf->filter_options.options = malloc((sizeof(char **) * 2*2)-1);
            if (!conf->filter_options.options)
                error_and_exit(errmsg, errno, NULL);
            conf->filter_options.options[0] = F_ZSTD_LEVEL;
            conf->filter_options.options[1] = the_num[3];
            conf->filter_options.options[2] = NULL;
    }
}

/* set_format_option - sets a specified option for the configuration.
 *
 * conf - a pointer to the struct configuration that will be modified
 * op - pointer to a string indicating the option we wish to modify
 * set - pointer to a string indicating what the option will be set to
 *
 */
void
set_format_option(struct configuration *conf,
                  const char *op,
                  const char *set,
                  const char *fstring)
{
    const char errmsg[] = "configuration.c:set_format_option";
    int i, olen;
    const char **ptr;

    if (!conf)
        cerror_and_exit(errmsg, "No struct configuration given.", NULL);
    if ((int)conf->format_options.type_id.archive_type == -1)
        cerror_and_exit(errmsg, "Attempted to set format option while format "
                        "is not set.", NULL);

    if (strncmp(conf->format_options.string_identifier, fstring,
                strlen(conf->format_options.string_identifier)))
        cerror_and_exit(errmsg,
                        "Setting option for invalid format: ",
                        fstring);
    olen = 0;
    ptr = conf->format_options.options;
    while (*(ptr++))
        olen++;
    for (i = 0; i < olen; i++) {
        if (!strcmp(op, conf->format_options.options[i]))
            break;
    }
    if (i == olen)
        cerror_and_exit(errmsg, "Attempted to set unrecognized option: ", op);

    verify_and_set_format(conf->format_options.type_id.archive_type,
                          conf->format_options.options + i, set);
}

/* set_filter_option - sets a specified option for the configuration.
 *
 * conf - a pointer to the struct configuration that will be modified
 * op - pointer to a string indicating the option we wish to modify
 * set - pointer to a string indicating what the option will be set to
 *
 */
void
set_filter_option(struct configuration *conf,
                  const char *op,
                  const char *set,
                  const char *fstring)
{
    const char errmsg[] = "configuration.c:set_filter_option";
    int i, olen;
    const char **ptr;

    if (!conf)
        cerror_and_exit(errmsg, "No struct configuration given.", NULL);
    if ((int)conf->filter_options.type_id.compression_format == -1)
        cerror_and_exit(errmsg, "Attempted to set format option while format "
                        "is not set.", NULL);

    if (strncmp(conf->filter_options.string_identifier, fstring,
                strlen(conf->filter_options.string_identifier)))
        cerror_and_exit(errmsg,
                        "Setting option for invalid filter: ",
                        fstring);
    olen = 0;
    ptr = conf->filter_options.options;
    while (*(ptr++))
        olen++;
    for (i = 0; i < olen; i++) {
        if (!strcmp(op, conf->filter_options.options[i]))
            break;
    }
    if (i == olen)
        cerror_and_exit(errmsg, "Attempted to set unrecognized option: ", op);

    verify_and_set_filter(conf->filter_options.type_id.compression_format,
                          conf->filter_options.options + i, set);
}

static void
verify_and_set_format(enum sarchive at, const char **arr, const char *set)
{
    const char errmsg[] = "configuration.c:verify_and_set_format";
    int i, slen, tmp;
    switch (at) {
        case tar:
            /* Currently nothing is supported for this format. */
            break;
        case s7zip:
            if (!strcmp(*arr, A_7Z_COMPRESSION)) {
                slen = sizeof(a_7z_compression) / sizeof(a_7z_compression[0]);
                for (i = 0; i < slen; i++) {
                    if (!strcmp(a_7z_compression[i], set))
                        break;
                }
                if (i == slen)
                    cerror_and_exit(errmsg, "Invalid compression type: ", set);
                arr[1] = a_7z_compression[i];
            } else if (!strcmp(*arr, A_7Z_LEVEL)) {
                errno = 0;
                tmp = strtol(set, NULL, 10);
                if (errno)
                    error_and_exit(errmsg, errno, set);
                if (tmp > INT_MAX || tmp < 0 || tmp > 9)
                    cerror_and_exit(errmsg, "Compression level is invalid: ",
                                    set);
                *(arr + 1) = the_num[tmp];
            }
            break;
        default:
            cerror_and_exit(errmsg, "Invalid archive format.", NULL);
    }
}

static void
verify_and_set_filter(enum compression ct, const char **arr, const char *set)
{
    const char errmsg[] = "configuration.c:verify_and_set_filter";
    int tmp;
    switch (ct) {
        case none:
            /* Should be no methods for none. */
            break;
        case xz:
            if (!strcmp(arr[0], F_XZ_LEVEL)) {
                errno = 0;
                tmp = strtol(set, NULL, 10);
                if (errno)
                    error_and_exit(errmsg, errno, set);
                if (tmp > INT_MAX || tmp < 0 || tmp > 9)
                    cerror_and_exit(errmsg, "Compression level is invalid: ",
                                    set);
                arr[1] = the_num[tmp];
            } else if (!strcmp(arr[0], F_XZ_THREADS)) {
                errno = 0;
                tmp = strtol(set, NULL, 10);
                if (errno)
                    error_and_exit(errmsg, errno, set);
                if (tmp > INT_MAX || tmp < 1 || tmp > 39)
                    cerror_and_exit(errmsg, "Compression threads is invalid: ",
                                    set);
                arr[1] = the_num[tmp];
            }
            break;
        case zstd:
            if (!strcmp(arr[0], F_ZSTD_LEVEL)) {
                errno = 0;
                tmp = strtol(set, NULL, 10);
                if (errno)
                    error_and_exit(errmsg, errno, set);
                if (tmp > INT_MAX || tmp < 1 || tmp > 22)
                    cerror_and_exit(errmsg, "Compression level is invalid: ",
                                    set);
                arr[1] = the_num[tmp];
            }
            break;
        default:
            cerror_and_exit(errmsg, "Invalid compression filter.", NULL);
    }
}

/* set_archive_options - dynamically creates a string that can be passed to
 * the archive_write_set_options function.
 */
void
set_archive_options(struct configuration *conf, char **out)
{
    const char errmsg[] = "configuration.c:set_archive_options";
    const char **tmp;
    char *ptr;
    int i, stringlength = 0;
    if (!conf)
        cerror_and_exit(errmsg, "Struct configuration is NULL.", NULL);
    /* First calculate required string length. */
    i = 0;
    if ((tmp = conf->format_options.options)) {
        while (tmp[i]) {
            stringlength += strlen(conf->format_options.string_identifier) + 1;
            stringlength += strlen(tmp[i]) + strlen(tmp[i+1]) + 2;
            i += 2;
        }
    }
    i = 0;
    if ((tmp = conf->filter_options.options)) {
        while (tmp[i]) {
            stringlength += strlen(conf->filter_options.string_identifier) + 1;
            stringlength += strlen(tmp[i]) + strlen(tmp[i+1]) + 2;
            i += 2;
        }
    }
    ptr = malloc(stringlength + 1);
    if (!ptr)
        error_and_exit(errmsg, errno, NULL);
    /* Now we create the actual string. */
    ptr[0] = '\0';
    i = 0;
    if ((tmp = conf->format_options.options)) {
        while (tmp[i]) {
            strcat(ptr, conf->format_options.string_identifier);
            strcat(ptr, ":");
            strcat(ptr, tmp[i]);
            strcat(ptr, "=");
            strcat(ptr, tmp[i+1]);
            strcat(ptr, ",");
            i += 2;
        }
    }
    i = 0;
    if ((tmp = conf->filter_options.options)) {
        while (tmp[i]) {
            strcat(ptr, conf->filter_options.string_identifier);
            strcat(ptr, ":");
            strcat(ptr, tmp[i]);
            strcat(ptr, "=");
            strcat(ptr, tmp[i+1]);
            strcat(ptr, ",");
            i += 2;
        }
    }
    if (stringlength)
        ptr[stringlength-1] = '\0';
    *out = ptr;
}
